<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>ConnectDialog</name>
    <message>
        <location filename="../client/src/connect_dialog.ui"/>
        <source>Connection settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>PIN code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>PIN codes must match on both clients.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>&amp;Local UDP port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Wai&amp;t for connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Connect &amp;to another client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>IP address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>&amp;UDP port:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../client/src/main_window.cpp" line="+54"/>
        <source>Disconnecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Connecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&lt;b&gt;Connected&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&lt;b&gt;Disconnected&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;b&gt;Received:&lt;/b&gt; %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&lt;b&gt;Sent:&lt;/b&gt; %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
