<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ConnectDialog</name>
    <message>
        <location filename="../client/src/connect_dialog.ui"/>
        <source>Connection settings</source>
        <translation>Настройки подключения</translation>
    </message>
    <message>
        <location/>
        <source>PIN code:</source>
        <translation>PIN-код:</translation>
    </message>
    <message>
        <location/>
        <source>PIN codes must match on both clients.</source>
        <translation>PIN-коды должны совпадать на обоих клиентах.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Local UDP port:</source>
        <translation>&amp;Локальный UPD-порт:</translation>
    </message>
    <message>
        <location/>
        <source>Wai&amp;t for connection</source>
        <translation>&amp;Ожидать подключения</translation>
    </message>
    <message>
        <location/>
        <source>Connect &amp;to another client</source>
        <translation>&amp;Подключиться к другому клиенту</translation>
    </message>
    <message>
        <location/>
        <source>IP address:</source>
        <translation>IP-адрес:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;UDP port:</source>
        <translation>&amp;UDP-порт:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../client/src/main_window.cpp" line="+98"/>
        <source>Connect</source>
        <translation>Подключиться</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Disconnect</source>
        <translation>Отключиться</translation>
    </message>
    <message>
        <location line="-34"/>
        <source>Disconnecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Connecting...</source>
        <translation>Подключение...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&lt;b&gt;Connected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Соединение установлено&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&lt;b&gt;Disconnected&lt;/b&gt;</source>
        <translation>&lt;b&gt;Соединение завершено&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;b&gt;Received:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Получено:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&lt;b&gt;Sent:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Отправлено:&lt;/b&gt; %1</translation>
    </message>
</context>
</TS>
