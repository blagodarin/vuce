#!/bin/sh
wget -q -O - https://dl.bintray.com/boostorg/release/1.67.0/source/boost_1_67_0.tar.bz2 | tar -xj --strip-components 1
case $1 in
  clang )
    ./bootstrap.sh --with-toolset=clang
    ./b2 --with-date_time --with-system --with-test address-model=64 cxxflags="-stdlib=libc++" link=static linkflags="-stdlib=libc++" threading=multi toolset=clang variant=$2
    ;;
esac
