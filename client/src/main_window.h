#pragma once

#include <QWidget>

#include "endpoint_signals.h"

class MainWindow : public QWidget
{
	Q_OBJECT

public:
	MainWindow();
	~MainWindow() override;

private:
	class Impl;
	const std::unique_ptr<Impl> _impl;
};
