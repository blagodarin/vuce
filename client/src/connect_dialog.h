#pragma once

#include <memory>

#include <QDialog>
#include <QHostAddress>

class Ui_ConnectDialog;

class ConnectDialog : public QDialog
{
	Q_OBJECT

public:
	explicit ConnectDialog(QWidget*);
	~ConnectDialog() noexcept override;

	std::uint16_t local_port() const;
	bool initiate_connection() const;
	std::uint32_t pin_code() const;
	QHostAddress remote_ip() const;
	std::uint16_t remote_port() const;

private:
	std::unique_ptr<Ui_ConnectDialog> _ui;
};
