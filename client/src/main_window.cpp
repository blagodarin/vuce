#include "main_window.h"

#include <uce/buffer.h>

#include "connect_dialog.h"
#include "ui_main_window.h"

class MainWindow::Impl
{
public:
	enum class EndpointState
	{
		Disconnected,
		Connecting,
		Connected,
	};

	Ui_MainWindow _ui;
	ConnectDialog _connection_dialog;
	EndpointSignals _endpoint_signals;
	std::unique_ptr<uce::Endpoint> _endpoint;
	std::shared_ptr<uce::Connection> _connection;
	EndpointState _endpoint_state = EndpointState::Disconnected;

	explicit Impl(MainWindow& main_window)
		: _connection_dialog{ &main_window }
	{
	}

	void create_endpoint()
	{
		_endpoint.reset();
		_endpoint = uce::create_endpoint(_connection_dialog.pin_code(), _connection_dialog.local_port(), _endpoint_signals);
	}
};

MainWindow::MainWindow()
	: _impl{ std::make_unique<Impl>(*this) }
{
	_impl->_ui.setupUi(this);

	connect(_impl->_ui.connection_button, &QPushButton::clicked, [this]
	{
		switch (_impl->_endpoint_state)
		{
		case Impl::EndpointState::Disconnected:
			_impl->_connection_dialog.open();
			break;

		case Impl::EndpointState::Connecting:
			break;

		case Impl::EndpointState::Connected:
			_impl->_ui.connection_button->setText(tr("Disconnecting..."));
			_impl->_ui.connection_button->setEnabled(false);
			_impl->_ui.send_edit->setEnabled(false);
			_impl->_endpoint->stop();
			break;
		}
	});

	connect(&_impl->_connection_dialog, &ConnectDialog::accepted, [this]
	{
		if (_impl->_connection_dialog.initiate_connection())
		{
			bool ok = false;
			const auto ip = _impl->_connection_dialog.remote_ip().toIPv4Address(&ok);
			if (!ok)
				return;
			_impl->create_endpoint();
			_impl->_endpoint->start({ static_cast<std::uint8_t>(ip >> 24), static_cast<std::uint8_t>(ip >> 16),
				static_cast<std::uint8_t>(ip >> 8), static_cast<std::uint8_t>(ip), _impl->_connection_dialog.remote_port() });
		}
		else
		{
			_impl->create_endpoint();
			_impl->_endpoint->start();
		}
		_impl->_endpoint_state = Impl::EndpointState::Connecting;
		_impl->_ui.connection_button->setText(tr("Connecting..."));
		_impl->_ui.connection_button->setEnabled(false);
	});

	connect(&_impl->_endpoint_signals, &EndpointSignals::connected, this, [this](const std::shared_ptr<uce::Connection>& connection)
	{
		_impl->_connection = connection;
		_impl->_endpoint_state = Impl::EndpointState::Connected;
		_impl->_ui.connection_button->setText(tr("Disconnect"));
		_impl->_ui.connection_button->setEnabled(true);
		_impl->_ui.log_text->append(tr("<b>Connected</b>"));
		_impl->_ui.send_edit->setEnabled(true);
	});

	connect(&_impl->_endpoint_signals, &EndpointSignals::disconnected, this, [this](const std::shared_ptr<uce::Connection>&)
	{
		_impl->_connection.reset();
		_impl->_endpoint_state = Impl::EndpointState::Disconnected;
		_impl->_ui.connection_button->setText(tr("Connect"));
		_impl->_ui.connection_button->setEnabled(true);
		_impl->_ui.log_text->append(tr("<b>Disconnected</b>"));
		_impl->_ui.send_edit->setEnabled(false);
	});

	connect(&_impl->_endpoint_signals, &EndpointSignals::received, this, [this](const std::shared_ptr<uce::Connection>&, const std::shared_ptr<uce::Buffer>& buffer)
	{
		const auto message = QString::fromUtf8(reinterpret_cast<char*>(buffer->data()), static_cast<int>(buffer->size()));
		_impl->_ui.log_text->append(tr("<b>Received:</b> %1").arg(message.toHtmlEscaped()));
	});

	connect(_impl->_ui.send_edit, &QLineEdit::returnPressed, [this]
	{
		const auto text = _impl->_ui.send_edit->text().trimmed();
		_impl->_ui.send_edit->clear();
		if (!text.isEmpty() && _impl->_connection)
		{
			const auto utf8 = text.toUtf8();
			if (_impl->_connection->send(uce::Buffer{ utf8.constData(), static_cast<std::size_t>(utf8.size()) }))
				_impl->_ui.log_text->append(tr("<b>Sent:</b> %1").arg(text.toHtmlEscaped()));
		}
	});
}

MainWindow::~MainWindow() = default;
