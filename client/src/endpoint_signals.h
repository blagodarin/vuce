#pragma once

#include <QObject>

#include <uce/endpoint.h>

Q_DECLARE_METATYPE(std::shared_ptr<uce::Buffer>)
Q_DECLARE_METATYPE(std::shared_ptr<uce::Connection>)

class EndpointSignals : public QObject, public uce::EndpointCallbacks
{
	Q_OBJECT

signals:
	void connected(const std::shared_ptr<uce::Connection>&);
	void disconnected(const std::shared_ptr<uce::Connection>&);
	void received(const std::shared_ptr<uce::Connection>&, const std::shared_ptr<uce::Buffer>&);

private:
	void on_connected(const std::shared_ptr<uce::Connection>&) override;
	void on_disconnected(const std::shared_ptr<uce::Connection>&) override;
	void on_received(const std::shared_ptr<uce::Connection>&, uce::Buffer&&) override;
};
