#include <QApplication>
#include <QTranslator>

#include "main_window.h"

int main(int argc, char** argv)
{
	QApplication app{ argc, argv };
	QTranslator translator;
	if (translator.load(QLocale{}, "client", "-", "translations"))
		app.installTranslator(&translator);
	MainWindow main_window;
	main_window.show();
	return app.exec();
}
