#include "endpoint_signals.h"

#include <uce/buffer.h>

namespace
{
	const int _meta_type_ids[]
	{
		qRegisterMetaType<std::shared_ptr<uce::Buffer>>(),
		qRegisterMetaType<std::shared_ptr<uce::Connection>>(),
	};
}

void EndpointSignals::on_connected(const std::shared_ptr<uce::Connection>& connection)
{
	emit connected(connection);
}

void EndpointSignals::on_disconnected(const std::shared_ptr<uce::Connection>& connection)
{
	emit disconnected(connection);
}

void EndpointSignals::on_received(const std::shared_ptr<uce::Connection>& connection, uce::Buffer&& buffer)
{
	emit received(connection, std::make_shared<uce::Buffer>(std::move(buffer)));
}
