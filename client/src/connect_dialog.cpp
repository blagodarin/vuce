#include "connect_dialog.h"

#include <QPushButton>

#include "ui_connect_dialog.h"

ConnectDialog::ConnectDialog(QWidget* parent)
	: QDialog{ parent }
	, _ui{ std::make_unique<Ui_ConnectDialog>() }
{
	_ui->setupUi(this);
	_ui->remote_ip_edit->setValidator(new QRegExpValidator{ QRegExp{ "(?:(?:25[0-5]|2[0-4]\\d|[2-9]\\d?|1\\d{,2}|0)\\.){3}(?:25[0-5]|2[0-4]\\d|[2-9]\\d?|1\\d{,2}|0)" }, _ui->remote_ip_edit });
	connect(_ui->remote_ip_edit, &QLineEdit::textChanged, [this]
	{
		const bool acceptable = _ui->remote_ip_edit->hasAcceptableInput();
		_ui->remote_ip_edit->setStyleSheet(acceptable ? QString{} : "background-color: #fdd");
		_ui->buttons->button(QDialogButtonBox::Ok)->setEnabled(acceptable);
	});
}

ConnectDialog::~ConnectDialog() noexcept = default;

std::uint16_t ConnectDialog::local_port() const
{
	return static_cast<std::uint16_t>(_ui->local_port_spin->value());
}

bool ConnectDialog::initiate_connection() const
{
	return _ui->connect_radio->isChecked();
}

std::uint32_t ConnectDialog::pin_code() const
{
	return _ui->pin_edit->text().toUInt();
}

QHostAddress ConnectDialog::remote_ip() const
{
	return QHostAddress{ _ui->remote_ip_edit->text() };
}

std::uint16_t ConnectDialog::remote_port() const
{
	return static_cast<std::uint16_t>(_ui->remote_port_spin->value());
}
