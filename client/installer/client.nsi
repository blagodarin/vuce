!include "MUI2.nsh"

Name "VideoProject Client"
OutFile "${INSTALLER_PATH}"
InstallDir "$PROGRAMFILES64\VideoProject Client"
RequestExecutionLevel admin

!define MUI_ABORTWARNING

!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "Russian"

!if ${CONFIG} == Debug
	!define SUFFIX d
!else
	!define SUFFIX
!endif

Section
	SetOutPath "$INSTDIR"
	WriteUninstaller "$INSTDIR\uninstall.exe"

	SetOutPath "$INSTDIR"
	File "${BUILD_DIR}\bin\${CONFIG}\client.exe"

	SetOutPath "$INSTDIR\translations"
	File "${BUILD_DIR}\translations\client-*.qm"

	SetOutPath "$INSTDIR"
	File "$%QTDIR%\bin\libEGL${SUFFIX}.dll"
	File "$%QTDIR%\bin\libGLESv2${SUFFIX}.dll"
	File "$%QTDIR%\bin\Qt5Core${SUFFIX}.dll"
	File "$%QTDIR%\bin\Qt5Gui${SUFFIX}.dll"
	File "$%QTDIR%\bin\Qt5Network${SUFFIX}.dll"
	File "$%QTDIR%\bin\Qt5Widgets${SUFFIX}.dll"

	SetOutPath "$INSTDIR\platforms"
	File "$%QTDIR%\plugins\platforms\qwindows${SUFFIX}.dll"

	SetOutPath "$INSTDIR\styles"
	File "$%QTDIR%\plugins\styles\qwindowsvistastyle${SUFFIX}.dll"
SectionEnd

Section "Uninstall"
	RMDir /r "$INSTDIR"
SectionEnd
