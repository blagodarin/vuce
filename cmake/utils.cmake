function(add_translations _target)
	if(NOT TARGET ${_target})
		message(SEND_ERROR "'${_target}' is not a target")
		return()
	endif()
	set(_multi_value_args UI_FILES)
	cmake_parse_arguments(_args "" "" "${_multi_value_args}" ${ARGN})
	set(_qm_dir ${PROJECT_BINARY_DIR}/translations)
	foreach(_locale en_US ru_RU)
		set(_ts_file ${PROJECT_SOURCE_DIR}/translations/${_target}-${_locale}.ts)
		set(_qm_file ${_qm_dir}/${_target}-${_locale}.qm)
		add_custom_command(OUTPUT ${_qm_file}
			COMMAND ${CMAKE_COMMAND} -E make_directory ${_qm_dir}
			COMMAND ${Qt5_LRELEASE_EXECUTABLE} -compress -nounfinished -removeidentical -silent ${_ts_file} -qm ${_qm_file}
			DEPENDS ${_ts_file}
			VERBATIM)
		list(APPEND _ts_files ${_ts_file})
		list(APPEND _qm_files ${_qm_file})
	endforeach()
	add_custom_target(${_target}_qm DEPENDS ${_qm_files})
	get_target_property(_folder ${_target} FOLDER)
	set_target_properties(${_target}_qm PROPERTIES FOLDER ${_folder})
	get_target_property(_source_dir ${_target} SOURCE_DIR)
	get_target_property(_sources ${_target} SOURCES)
	add_custom_command(TARGET ${_target} POST_BUILD
		COMMAND ${Qt5_LUPDATE_EXECUTABLE} -locations relative -no-obsolete -no-ui-lines -silent ${_sources} ${_args_UI_FILES} -ts ${_ts_files}
		BYPRODUCTS ${_ts_files}
		DEPENDS ${_sources} ${_args_UI_FILES}
		VERBATIM
		WORKING_DIRECTORY ${_source_dir})
endfunction()

function(append_options _variable)
	string(REPLACE ";" " " _value "${ARGN}")
	if(NOT "${${_variable}}" STREQUAL "")
		set(_separator " ")
	else()
		set(_separator "")
	endif()
	set(${_variable} "${${_variable}}${_separator}${_value}" PARENT_SCOPE)
endfunction()

function(print_list _prefix)
	message(STATUS "${_prefix}")
	if(NOT "${ARGN}" STREQUAL "")
		string(REPLACE ";" " " text " ${ARGN}")
		message(STATUS " ${text}")
	else()
		message(STATUS "  (none)")
	endif()
endfunction()

function(register_test _target)
	if(NOT TARGET ${_target})
		message(SEND_ERROR "'${_target}' is not a target")
		return()
	endif()
	get_property(_target_type TARGET ${_target} PROPERTY TYPE)
	if(NOT "${_target_type}" STREQUAL "EXECUTABLE")
		message(SEND_ERROR "'${_target}' is not an executable")
		return()
	endif()
	add_custom_command(TARGET ${_target} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E env ASAN_OPTIONS=allocator_may_return_null=1 $<TARGET_FILE:${_target}> --log_level=message
		VERBATIM)
endfunction()
