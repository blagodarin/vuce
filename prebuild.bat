@echo off

if "%~1"=="boost" (
	if "%BOOST_ROOT%"=="" (
		echo Please define BOOST_ROOT.
		goto :eof
	)
	echo --------------------------------------------------------------------------------
	echo Building Boost with BOOST_ROOT=%BOOST_ROOT%
	echo --------------------------------------------------------------------------------
	pushd "%BOOST_ROOT%"
	call bootstrap.bat
	b2.exe headers
	b2.exe -j2 --with-date_time --with-system --with-test address-model=64 link=static threading=multi
	popd
	goto :eof
)

if "%~1"=="qt" (
	if "%QTDIR%"=="" (
		echo Please define QTDIR.
		goto :eof
	)
	if "%VCINSTALLDIR%"=="" (
		call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat"
	)
	echo --------------------------------------------------------------------------------
	echo Building Qt with QTDIR=%QTDIR%
	echo --------------------------------------------------------------------------------
	pushd "%QTDIR%"
	call configure.bat -prefix "%QTDIR%" -opensource -confirm-license -force-debug-info -nomake examples -no-dbus -no-sql-odbc -no-sql-sqlite
	nmake
	popd
	goto :eof
)

echo Prerequisite builder.
echo Usage:
echo     %~nx0          Show this help.
echo     %~nx0 boost    Build Boost. Requires BOOST_ROOT to be set.
echo     %~nx0 qt       Build Qt. Requires QTDIR to be set.
