#pragma once

#include <array>
#include <memory>

namespace uce
{
	class Buffer;

	class UdpAddress
	{
	public:
		std::array<std::uint8_t, 4> _ip{ 0, 0, 0, 0 };
		std::uint16_t _port = 0;

		constexpr UdpAddress() noexcept = default;
		constexpr UdpAddress(std::uint8_t a, std::uint8_t b, std::uint8_t c, std::uint8_t d, std::uint16_t port) noexcept : _ip{ a, b, c, d }, _port{ port } {}
	};

	class Connection
	{
	public:
		virtual ~Connection() noexcept = default;

		// Requests the connection to close.
		// Returns true if the close request has been accepted.
		// No requests are accepted after a close request.
		virtual bool close() = 0;

		// Requests the connection to send a message.
		// Returns true if the send request has been accepted.
		virtual bool send(Buffer&&) = 0;
	};

	class Endpoint
	{
	public:
		virtual ~Endpoint() noexcept = default;

		// Limits the total size of outbound buffers for a connection.
		virtual void set_outbound_threshold(std::size_t bytes) = 0;

		// Starts communication as a passive endpoint.
		// The endpoint will wait for a connection request from another (active) endpoint.
		// If already started, does nothing.
		virtual void start() = 0;

		// Starts communication as an active endpoint.
		// If not connected, starts sending connection requests to the specified address.
		// If connected, initiates roaming negotiations.
		virtual void start(UdpAddress const&) = 0;

		// Stops communication..
		virtual void stop() = 0;
	};

	// Asynchronous callbacks for endpoint events.
	// No callbacks are called until the endpoint is started.
	class EndpointCallbacks
	{
	public:
		virtual ~EndpointCallbacks() noexcept = default;

		virtual void on_connected(std::shared_ptr<Connection> const&) = 0;
		virtual void on_disconnected(std::shared_ptr<Connection> const&) = 0;
		virtual void on_received(std::shared_ptr<Connection> const&, Buffer&&) = 0;
	};

	// Creates an Endpoint with the specified connection token and binds it to the specified UDP port.
	// Only an Endpoint with the same connection token will be able connect to this endpoint.
	std::unique_ptr<Endpoint> create_endpoint(std::uint32_t token, std::uint16_t port, EndpointCallbacks&);
}
