#include <uce/buffer.h>

#include <array>

#include <boost/test/unit_test.hpp>

#include "helpers.h"

BOOST_AUTO_TEST_CASE(test_buffer)
{
	std::array<std::uint8_t, 4> const bytes{ 0x00, 0x01, 0x02, 0x03 };
	BOOST_TEST_CONTEXT("Empty buffer construction")
	{
		uce::Buffer const a;
		BOOST_CHECK_EQUAL(0u, a.capacity());
		BOOST_CHECK_EQUAL(nullptr, a.data());
		BOOST_CHECK(a.empty());
		BOOST_CHECK_EQUAL(0u, a.size());

		uce::Buffer const b{ nullptr, 0 };
		BOOST_CHECK_EQUAL(0u, b.capacity());
		BOOST_CHECK_EQUAL(nullptr, b.data());
		BOOST_CHECK(b.empty());
		BOOST_CHECK_EQUAL(0u, b.size());

		uce::Buffer const c{ bytes.data(), 0 };
		BOOST_CHECK_EQUAL(0u, c.capacity());
		BOOST_CHECK_EQUAL(nullptr, c.data());
		BOOST_CHECK(c.empty());
		BOOST_CHECK_EQUAL(0u, c.size());
	}
	BOOST_TEST_CONTEXT("Empty buffer comparison")
	{
		BOOST_CHECK(uce::Buffer{} == uce::Buffer{});
		BOOST_CHECK(uce::Buffer(nullptr, 0) == uce::Buffer(nullptr, 0));
		BOOST_CHECK(uce::Buffer(nullptr, 0) == uce::Buffer());
	}
	BOOST_TEST_CONTEXT("Equal buffer comparison")
	{
		uce::Buffer const a{ bytes.data(), bytes.size() };
		BOOST_CHECK(a == a);
		BOOST_CHECK(!(a != a));

		uce::Buffer const b{ bytes.data(), bytes.size() };
		BOOST_CHECK(a == b);
		BOOST_CHECK(!(a != b));
		BOOST_CHECK(a.capacity() == b.capacity());
		BOOST_CHECK(a.data() != b.data());
		BOOST_CHECK(a.size() == b.size());
	}
	BOOST_TEST_CONTEXT("Inequal buffer comparison")
	{
		uce::Buffer const a{ bytes.data(), 2 };
		uce::Buffer const b{ bytes.data(), 3 };
		BOOST_CHECK(a != b);

		uce::Buffer const c{ bytes.data() + 1, 2 };
		BOOST_CHECK(a != c);
		BOOST_CHECK(b != c);
	}
	BOOST_TEST_CONTEXT("Buffer move construction")
	{
		uce::Buffer a{ bytes.data(), bytes.size() };
		uce::Buffer const b{ std::move(a) };
		BOOST_CHECK(a.empty());
		BOOST_CHECK_EQUAL(uce::Buffer(bytes.data(), bytes.size()), b);
	}
	BOOST_TEST_CONTEXT("Buffer move assignment")
	{
		uce::Buffer a{ bytes.data(), bytes.size() };
		uce::Buffer b{ bytes.data(), 2 };
		b = std::move(a);
		BOOST_CHECK(a.empty());
		BOOST_CHECK_EQUAL(uce::Buffer(bytes.data(), bytes.size()), b);
	}
	BOOST_TEST_CONTEXT("Should throw std::bad_alloc when size is too large")
	{
		BOOST_CHECK_THROW(uce::Buffer(nullptr, std::numeric_limits<std::size_t>::max()), std::bad_alloc);
	}
}

BOOST_AUTO_TEST_CASE(test_buffer_by_reference)
{
	std::array<std::uint8_t, 4> bytes{ 0x00, 0x01, 0x02, 0x03 };
	BOOST_TEST_CONTEXT("Should refer to existing data")
	{
		auto const buffer = uce::Buffer::by_reference(bytes.data(), bytes.size());

		BOOST_CHECK_EQUAL(0u, buffer.capacity());
		BOOST_CHECK_EQUAL(reinterpret_cast<std::byte*>(bytes.data()), buffer.data());
		BOOST_CHECK(!buffer.empty());
		BOOST_CHECK_EQUAL(bytes.size(), buffer.size());
		BOOST_CHECK_EQUAL(uce::Buffer(bytes.data(), bytes.size()), buffer);
	}
	BOOST_TEST_CONTEXT("Should clean up on reset")
	{
		auto buffer = uce::Buffer::by_reference(bytes.data(), bytes.size());
		buffer.reset();

		BOOST_CHECK_EQUAL(0u, buffer.capacity());
		BOOST_CHECK_EQUAL(nullptr, buffer.data());
		BOOST_CHECK(buffer.empty());
		BOOST_CHECK_EQUAL(0u, buffer.size());
	}
	BOOST_TEST_CONTEXT("Should allocate on reset")
	{
		auto buffer = uce::Buffer::by_reference(bytes.data(), bytes.size());
		buffer.reset(bytes.size());

		BOOST_CHECK_EQUAL(bytes.size(), buffer.capacity());
		BOOST_CHECK_NE(reinterpret_cast<std::byte*>(bytes.data()), buffer.data());
		BOOST_CHECK(!buffer.empty());
		BOOST_CHECK_EQUAL(bytes.size(), buffer.size());
	}
	BOOST_TEST_CONTEXT("Should not allocate on move construction")
	{
		auto buffer = uce::Buffer::by_reference(bytes.data(), bytes.size());
		uce::Buffer other{ std::move(buffer) };

		BOOST_CHECK_EQUAL(0u, buffer.capacity());
		BOOST_CHECK_EQUAL(nullptr, buffer.data());
		BOOST_CHECK(buffer.empty());
		BOOST_CHECK_EQUAL(0u, buffer.size());

		BOOST_CHECK_EQUAL(0u, other.capacity());
		BOOST_CHECK_EQUAL(reinterpret_cast<std::byte*>(bytes.data()), other.data());
		BOOST_CHECK(!other.empty());
		BOOST_CHECK_EQUAL(bytes.size(), other.size());
	}
	BOOST_TEST_CONTEXT("Should not allocate on move assignment")
	{
		auto buffer = uce::Buffer::by_reference(bytes.data(), bytes.size());
		uce::Buffer other;
		other = std::move(buffer);

		BOOST_CHECK_EQUAL(0u, buffer.capacity());
		BOOST_CHECK_EQUAL(nullptr, buffer.data());
		BOOST_CHECK(buffer.empty());
		BOOST_CHECK_EQUAL(0u, buffer.size());

		BOOST_CHECK_EQUAL(0u, other.capacity());
		BOOST_CHECK_EQUAL(reinterpret_cast<std::byte*>(bytes.data()), other.data());
		BOOST_CHECK(!other.empty());
		BOOST_CHECK_EQUAL(bytes.size(), other.size());
	}
}

BOOST_AUTO_TEST_CASE(test_buffer_reset)
{
	uce::Buffer buffer;
	BOOST_CHECK_EQUAL(0u, buffer.capacity());
	BOOST_CHECK_EQUAL(0u, buffer.size());

	buffer.reset(10);
	BOOST_CHECK_LE(10u, buffer.capacity());
	BOOST_CHECK_EQUAL(10u, buffer.size());
	buffer.data()[9] = std::byte{ 10 };

	buffer.reset(30);
	BOOST_CHECK_LE(30u, buffer.capacity());
	BOOST_CHECK_EQUAL(30u, buffer.size());
	buffer.data()[29] = std::byte{ 30 };

	buffer.reset(20);
	BOOST_CHECK_LE(20u, buffer.capacity());
	BOOST_CHECK_EQUAL(20u, buffer.size());
	buffer.data()[19] = std::byte{ 20 };

	BOOST_CHECK_THROW(buffer.reset(std::numeric_limits<std::size_t>::max()), std::bad_alloc);
	BOOST_CHECK_EQUAL(nullptr, buffer.data());
	BOOST_CHECK_EQUAL(0u, buffer.capacity());
	BOOST_CHECK_EQUAL(0u, buffer.size());
}

BOOST_AUTO_TEST_CASE(test_buffer_self_move_assignment)
{
	std::array<std::uint8_t, 4> const bytes{ 0x00, 0x01, 0x02, 0x03 };
	uce::Buffer buffer{ bytes.data(), bytes.size() };
	auto const data = buffer.data();
	auto const capacity = buffer.capacity();
	auto const size = buffer.size();
	buffer = std::move(buffer);
	BOOST_CHECK_EQUAL(data, buffer.data());
	BOOST_CHECK_EQUAL(capacity, buffer.capacity());
	BOOST_CHECK_EQUAL(size, buffer.size());
}
