#include <condition_variable>
#include <deque>
#include <mutex>

#include <boost/test/unit_test.hpp>

#include <uce/buffer.h>
#include <uce/endpoint.h>
#include "helpers.h"

namespace
{
	std::mutex _mutex; // Boost.Test has mutable global state.

	class EndpointTester : public uce::EndpointCallbacks
	{
	public:
		explicit EndpointTester(std::string_view name) : _name{ name } {}
		~EndpointTester() noexcept override;

		auto create_endpoint(std::uint16_t port)
		{
			auto endpoint = uce::create_endpoint(0x00123456, port, *this);
			BOOST_REQUIRE(endpoint);
			return endpoint;
		}

		void expect_connected()
		{
			std::unique_lock lock{ _mutex };
			BOOST_TEST_CONTEXT(_name << " should connect")
			{
				BOOST_REQUIRE(_condition.wait_for(lock, std::chrono::milliseconds{ 100 }, [this] { return !_events.empty(); }));
				BOOST_REQUIRE(Event::OnConnected == _events.front().first);
				_events.pop_front();
			}
		}

		void expect_disconnected()
		{
			std::unique_lock lock{ _mutex };
			BOOST_TEST_CONTEXT(_name << " should disconnect")
			{
				BOOST_REQUIRE(_condition.wait_for(lock, std::chrono::milliseconds{ 100 }, [this] { return !_events.empty(); }));
				BOOST_REQUIRE(Event::OnDisconnected == _events.front().first);
				_events.pop_front();
			}
		}

		void expect_idle()
		{
			std::unique_lock lock{ _mutex };
			BOOST_TEST_CONTEXT(_name << " should be idle")
			{
				BOOST_REQUIRE(!_condition.wait_for(lock, std::chrono::milliseconds{ 100 }, [this] { return !_events.empty(); }));
			}
		}

		void expect_received(const std::vector<std::uint8_t>& bytes)
		{
			std::unique_lock lock{ _mutex };
			BOOST_TEST_CONTEXT(_name << " should receive a message")
			{
				BOOST_REQUIRE(_condition.wait_for(lock, std::chrono::milliseconds{ 100 }, [this] { return !_events.empty(); }));
				BOOST_REQUIRE(Event::OnReceived == _events.front().first);
				BOOST_REQUIRE_EQUAL(uce::Buffer(bytes.data(), bytes.size()), _events.front().second);
				_events.pop_front();
			}
		}

		void when_connected(std::function<void(const std::shared_ptr<uce::Connection>&)>&& callback)
		{
			std::scoped_lock lock{ _mutex };
			_on_connected = std::move(callback);
		}

		void when_disconnected(std::function<void(const std::shared_ptr<uce::Connection>&)>&& callback)
		{
			std::scoped_lock lock{ _mutex };
			_on_disconnected = std::move(callback);
		}

		void when_received(std::function<void(const std::shared_ptr<uce::Connection>&, uce::Buffer&&)>&& callback)
		{
			std::scoped_lock lock{ _mutex };
			_on_received = std::move(callback);
		}

	private:
		void on_connected(const std::shared_ptr<uce::Connection>& connection) override
		{
			std::unique_lock lock{ _mutex };
			BOOST_TEST_CONTEXT(_name << " has connected")
			{
				_events.emplace_back(Event::OnConnected, uce::Buffer{});
				if (_on_connected)
					_on_connected(connection);
			}
			lock.unlock();
			_condition.notify_one();
		}

		void on_disconnected(const std::shared_ptr<uce::Connection>& connection) override
		{
			std::unique_lock lock{ _mutex };
			BOOST_TEST_CONTEXT(_name << " has disconnected")
			{
				_events.emplace_back(Event::OnDisconnected, uce::Buffer{});
				if (_on_disconnected)
					_on_disconnected(connection);
			}
			lock.unlock();
			_condition.notify_one();
		}

		void on_received(const std::shared_ptr<uce::Connection>& connection, uce::Buffer&& message) override
		{
			std::unique_lock lock{ _mutex };
			BOOST_TEST_CONTEXT(_name << " has received a message")
			{
				_events.emplace_back(Event::OnReceived, uce::Buffer{ message.data(), message.size() });
				if (_on_received)
					_on_received(connection, std::move(message));
			}
			lock.unlock();
			_condition.notify_one();
		}

	private:
		enum class Event
		{
			OnConnected,
			OnDisconnected,
			OnReceived,
		};

		const std::string _name;
		std::function<void(const std::shared_ptr<uce::Connection>&)> _on_connected;
		std::function<void(const std::shared_ptr<uce::Connection>&)> _on_disconnected;
		std::function<void(const std::shared_ptr<uce::Connection>&, uce::Buffer&&)> _on_received;
		std::deque<std::pair<Event, uce::Buffer>> _events;
		std::condition_variable _condition;
	};

	EndpointTester::~EndpointTester() noexcept = default;

	bool send_bytes(const std::shared_ptr<uce::Connection>& connection, const std::vector<std::uint8_t>& bytes)
	{
		return connection->send(uce::Buffer{ bytes.data(), bytes.size() });
	}
}

BOOST_AUTO_TEST_CASE(test_endpoint_close_on_connect)
{
	BOOST_TEST_CONTEXT("Client-side")
	{
		EndpointTester server{ "Server" };
		server.when_disconnected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(!connection->close());
		});

		EndpointTester client{ "Client" };
		client.when_connected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(connection->close());
		});
		client.when_disconnected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(!connection->close());
		});

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		client.expect_disconnected();
		server.expect_disconnected();

		client.expect_idle();
		server.expect_idle();
	}
	BOOST_TEST_CONTEXT("Server-side")
	{
		EndpointTester server{ "Server" };
		server.when_connected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(connection->close());
		});
		server.when_disconnected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(!connection->close());
		});

		EndpointTester client{ "Client" };
		client.when_disconnected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(!connection->close());
		});

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		client.expect_disconnected();
		server.expect_disconnected();

		client.expect_idle();
		server.expect_idle();
	}
}

BOOST_AUTO_TEST_CASE(test_endpoint_close_on_receive)
{
	BOOST_TEST_CONTEXT("Client-side")
	{
		EndpointTester server{ "Server" };
		server.when_connected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(::send_bytes(connection, { 0x10 }));
		});

		EndpointTester client{ "Client" };
		client.when_received([](const std::shared_ptr<uce::Connection>& connection, uce::Buffer&&)
		{
			BOOST_CHECK(connection->close());
		});

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		client.expect_received({ 0x10 });

		client.expect_disconnected();
		server.expect_disconnected();

		client.expect_idle();
		server.expect_idle();
	}
	BOOST_TEST_CONTEXT("Server-side")
	{
		EndpointTester server{ "Server" };
		server.when_received([](const std::shared_ptr<uce::Connection>& connection, uce::Buffer&&)
		{
			BOOST_CHECK(connection->close());
		});

		EndpointTester client{ "Client" };
		client.when_connected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(::send_bytes(connection, { 0x10 }));
		});

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		server.expect_received({ 0x10 });

		client.expect_disconnected();
		server.expect_disconnected();

		client.expect_idle();
		server.expect_idle();
	}
}

BOOST_AUTO_TEST_CASE(test_endpoint_send_when_connected)
{
	BOOST_TEST_CONTEXT("Client-side")
	{
		EndpointTester server{ "Server" };

		EndpointTester client{ "Client" };
		client.when_connected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(::send_bytes(connection, { 0x10 }));
			BOOST_CHECK(::send_bytes(connection, { 0x20 }));
		});

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		server.expect_received({ 0x10 });
		server.expect_received({ 0x20 });

		client.expect_idle();
		server.expect_idle();
	}
	BOOST_TEST_CONTEXT("Server-side")
	{
		EndpointTester server{ "Server" };
		server.when_connected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(::send_bytes(connection, { 0x10 }));
			BOOST_CHECK(::send_bytes(connection, { 0x20 }));
		});

		EndpointTester client{ "Client" };

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		client.expect_received({ 0x10 });
		client.expect_received({ 0x20 });

		client.expect_idle();
		server.expect_idle();
	}
}

BOOST_AUTO_TEST_CASE(test_endpoint_send_when_disconnected)
{
	BOOST_TEST_CONTEXT("Client-side")
	{
		EndpointTester server{ "Server" };
		server.when_connected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(connection->close());
		});

		EndpointTester client{ "Client" };
		client.when_disconnected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(!::send_bytes(connection, { 0x10 }));
		});

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		client.expect_disconnected();
		server.expect_disconnected();

		client.expect_idle();
		server.expect_idle();
	}
	BOOST_TEST_CONTEXT("Server-side")
	{
		EndpointTester server{ "Server" };
		server.when_disconnected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(!::send_bytes(connection, { 0x10 }));
		});

		EndpointTester client{ "Client" };
		client.when_connected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(connection->close());
		});

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		client.expect_disconnected();
		server.expect_disconnected();

		client.expect_idle();
		server.expect_idle();
	}
}

BOOST_AUTO_TEST_CASE(test_endpoint_set_outbound_threshold)
{
	BOOST_TEST_CONTEXT("Client-side")
	{
		EndpointTester server{ "Server" };

		EndpointTester client{ "Client" };
		client.when_connected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(::send_bytes(connection, { 0x10 }));
			BOOST_CHECK(!::send_bytes(connection, { 0x20 }));
		});

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		client_endpoint->set_outbound_threshold(1);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		server.expect_received({ 0x10 });

		client.expect_idle();
		server.expect_idle();
	}
	BOOST_TEST_CONTEXT("Server-side")
	{
		EndpointTester server{ "Server" };
		server.when_connected([](const std::shared_ptr<uce::Connection>& connection)
		{
			BOOST_CHECK(::send_bytes(connection, { 0x10 }));
			BOOST_CHECK(!::send_bytes(connection, { 0x20 }));
		});

		EndpointTester client{ "Client" };

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		server_endpoint->set_outbound_threshold(1);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		client.expect_received({ 0x10 });

		client.expect_idle();
		server.expect_idle();
	}
}

BOOST_AUTO_TEST_CASE(test_endpoint_stop)
{
	BOOST_TEST_CONTEXT("Client-side")
	{
		EndpointTester server{ "Server" };

		EndpointTester client{ "Client" };

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		client_endpoint->set_outbound_threshold(1);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		client_endpoint->stop();

		client.expect_disconnected();
		server.expect_disconnected();

		client_endpoint->stop();

		client.expect_idle();
		server.expect_idle();
	}
	BOOST_TEST_CONTEXT("Server-side")
	{
		EndpointTester server{ "Server" };

		EndpointTester client{ "Client" };

		const auto server_endpoint = server.create_endpoint(10000);
		const auto client_endpoint = client.create_endpoint(10001);

		client_endpoint->set_outbound_threshold(1);

		server_endpoint->start();
		client_endpoint->start({ 127, 0, 0, 1, 10000 });

		client.expect_connected();
		server.expect_connected();

		server_endpoint->stop();

		client.expect_disconnected();
		server.expect_disconnected();

		server_endpoint->stop();

		client.expect_idle();
		server.expect_idle();
	}
}
