#include <boost/test/unit_test.hpp>

#include <uce/src/assert.h>
#include "helpers.h"
#include "protocol.h"

BOOST_FIXTURE_TEST_CASE(test_send_disconnected, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should fail to send while not connected")
	{
		BOOST_CHECK(!send_message({ 0x10 }));
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_failure, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should drop message if unable to send")
	{
		with(_fail_send, true, [this]
		{
			BOOST_CHECK(send_message({ 0x10 }));
		});
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_invalid_message, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should fail to send an empty message")
	{
		BOOST_CHECK(!send_message({}));
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	if constexpr (sizeof(std::size_t) > sizeof(std::uint32_t))
	{
		BOOST_TEST_CONTEXT("Should fail to send a message of 2^32 bytes")
		{
			BOOST_CHECK(!protocol().send_message(uce::Buffer::by_reference(nullptr, std::size_t{ std::numeric_limits<std::uint32_t>::max() } + 1)));
			check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
		}
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_message, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send message")
	{
		BOOST_CHECK(send_message({ 0x10, 0x20 }));
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10, 0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_message_fragments, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send fragment 1/2")
	{
		protocol().sender().set_max_fragment_size(1);
		BOOST_CHECK(send_message({ 0x10, 0x20 }));
		expect_sent(Address{ 1 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(2), FragmentOffset(0), Bytes(0x10) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send fragment 2/2")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		expect_sent(Address{ 1 }, { Prefix(Fragment), SequenceNumber(2), MessageSize(2), FragmentOffset(1), Bytes(0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 3 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should accept Ack")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(2) });
		check_state(Connected, Address{ 1 }, SeqNum{ 3 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_message_fragments_failure, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send fragment 1/2")
	{
		protocol().sender().set_max_fragment_size(1);
		BOOST_CHECK(send_message({ 0x10, 0x20 }));
		expect_sent(Address{ 1 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(2), FragmentOffset(0), Bytes(0x10) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should fail to send fragment 2/2")
	{
		with(_fail_send, true, [this]
		{
			receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		});
		expect_sent(Address{ 1 }, { Prefix(Fragment), SequenceNumber(2), MessageSize(2), FragmentOffset(1), Bytes(0x20) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_message_fragments_failure_queued, ProtocolTestFixture)
{
	protocol().sender().set_max_fragment_size(1);
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should send message 1")
	{
		BOOST_CHECK(send_message({ 0x10 }));
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should queue message 2")
	{
		BOOST_CHECK(send_message({ 0x20, 0x30 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(1, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should fail to send fragment 1/2")
	{
		with(_fail_send, true, [this]
		{
			receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		});
		expect_sent(Address{ 1 }, { Prefix(Fragment), SequenceNumber(2), MessageSize(2), FragmentOffset(0), Bytes(0x20) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_queue_decimation, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send message")
	{
		BOOST_CHECK(send_message({ 0x10, 0x20 }));
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10, 0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should queue message")
	{
		BOOST_CHECK(send_message({ 0x30, 0x40 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(1, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should queue message")
	{
		BOOST_CHECK(send_message({ 0x50, 0x60 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(2, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should drop queued message")
	{
		protocol().sender().set_outbound_threshold(4);
		BOOST_CHECK_EQUAL(1, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should unqueue and send message")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(2), Bytes(0x50, 0x60) });
		check_state(Connected, Address{ 1 }, SeqNum{ 3 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_queue_overflow, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send message")
	{
		protocol().sender().set_outbound_threshold(4);
		BOOST_CHECK(send_message({ 0x10, 0x20 }));
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10, 0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should queue message")
	{
		BOOST_CHECK(send_message({ 0x30, 0x40 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(1, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should queue message")
	{
		BOOST_CHECK(send_message({ 0x50, 0x60 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(1, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should unqueue and send message")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(2), Bytes(0x50, 0x60) });
		check_state(Connected, Address{ 1 }, SeqNum{ 3 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_queue_threshold, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send message")
	{
		protocol().sender().set_outbound_threshold(1);
		BOOST_CHECK(send_message({ 0x10, 0x20 }));
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10, 0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should fail to queue message")
	{
		BOOST_CHECK(!send_message({ 0x30 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should still fail to queue message")
	{
		protocol().sender().set_outbound_threshold(2);
		BOOST_CHECK(!send_message({ 0x40 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_queued_message_fragments, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send the first fragment of the first message")
	{
		protocol().sender().set_max_fragment_size(1);
		BOOST_CHECK(send_message({ 0x10, 0x20 }));
		expect_sent(Address{ 1 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(2), FragmentOffset(0), Bytes(0x10) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should queue the second message")
	{
		BOOST_CHECK(send_message({ 0x30, 0x40, 0x50 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send the second fragment of the first message")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		expect_sent(Address{ 1 }, { Prefix(Fragment), SequenceNumber(2), MessageSize(2), FragmentOffset(1), Bytes(0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 3 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send the first fragment of the second message")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(2) });
		expect_sent(Address{ 1 }, { Prefix(Fragment), SequenceNumber(3), MessageSize(3), FragmentOffset(0), Bytes(0x30) });
		check_state(Connected, Address{ 1 }, SeqNum{ 4 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send the second fragment of the second message")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(3) });
		expect_sent(Address{ 1 }, { Prefix(Fragment), SequenceNumber(4), MessageSize(3), FragmentOffset(1), Bytes(0x40) });
		check_state(Connected, Address{ 1 }, SeqNum{ 5 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send the third fragment of the second message")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(4) });
		expect_sent(Address{ 1 }, { Prefix(Fragment), SequenceNumber(5), MessageSize(3), FragmentOffset(2), Bytes(0x50) });
		check_state(Connected, Address{ 1 }, SeqNum{ 6 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should accept the acknowlegdement")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(5) });
		check_state(Connected, Address{ 1 }, SeqNum{ 6 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_send_queued_messages, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send message")
	{
		BOOST_CHECK(send_message({ 0x10, 0x20 }));
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10, 0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should queue message")
	{
		BOOST_CHECK(send_message({ 0x30, 0x40 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(1, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should queue message")
	{
		BOOST_CHECK(send_message({ 0x50, 0x60 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(2, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should ignore Ack from another address")
	{
		receive_packet(Address{ 2 }, { Prefix(Ack), SequenceNumber(1) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(2, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should ignore Ack packet with sequence number from past")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(0) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(2, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should ignore Ack packet with extra payload")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1), Bytes(0x00) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(2, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should ignore Ack packet with sequence number from future")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(2) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(2, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should unqueue and send message")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(2), Bytes(0x30, 0x40) });
		check_state(Connected, Address{ 1 }, SeqNum{ 3 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(1, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should disconnect if failed to unqueue and send message")
	{
		with(_fail_send, true, [this]
		{
			receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(2) });
		});
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(3), Bytes(0x50, 0x60) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
}
