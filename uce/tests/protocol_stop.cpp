#include <boost/test/unit_test.hpp>

#include "helpers.h"
#include "protocol.h"

BOOST_FIXTURE_TEST_CASE(test_stop_accepting, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start accepting")
	{
		protocol().helper().set_nonce(5);
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(3) });
		expect_sent(Address{ 1 }, { Prefix(Accept), Nonce(3), Nonce(5) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		BOOST_CHECK(protocol().stop());
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_stop_connected, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should start disconnecting")
	{
		BOOST_CHECK(!protocol().stop());
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_stop_connected_failure, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		with(_fail_send, true, [this]
		{
			BOOST_CHECK(protocol().stop());
		});
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_stop_connecting, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().helper().set_nonce(3);
		protocol().set_remote_address(Address{ 1 });
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(3) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		BOOST_CHECK(protocol().stop());
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_stop_disconnected, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should be disconnected")
	{
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should do nothing")
	{
		BOOST_CHECK(protocol().stop());
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_stop_disconnecting, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start disconnecting")
	{
		initiate_connection(Address{ 1 });
		protocol().disconnect();
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should continue disconnecting")
	{
		BOOST_CHECK(!protocol().stop());
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
}
