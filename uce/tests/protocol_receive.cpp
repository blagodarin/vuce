#include <boost/test/unit_test.hpp>

#include <uce/src/assert.h>
#include "helpers.h"
#include "protocol.h"

BOOST_FIXTURE_TEST_CASE(test_receive, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should ignore Message if not connected")
	{
		receive_packet(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should connect")
	{
		accept_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Message from another address")
	{
		receive_packet(Address{ 2 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should accept Message")
	{
		receive_packet(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10) });
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		expect_received({ 0x10 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 1 });
	}
	BOOST_TEST_CONTEXT("Should fail to accept Message")
	{
		with(_fail_send, true, [this]
		{
			receive_packet(Address{ 1 }, { Prefix(Message), SequenceNumber(2), Bytes(0x20) });
		});
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(2) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_receive_fragments, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should ignore Fragment if not connected")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(6), FragmentOffset(0), Bytes(0x10) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should connect")
	{
		accept_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Fragment from another address")
	{
		receive_packet(Address{ 2 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(6), FragmentOffset(0), Bytes(0x10) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should accept Fragment 1/3")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(6), FragmentOffset(0), Bytes(0x10) });
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 1 });
	}
	BOOST_TEST_CONTEXT("Should accept Fragment 2/3")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(2), MessageSize(6), FragmentOffset(1), Bytes(0x20, 0x30) });
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(2) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 2 });
	}
	BOOST_TEST_CONTEXT("Should accept Fragment 3/3")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(3), MessageSize(6), FragmentOffset(3), Bytes(0x40, 0x50, 0x60) });
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(3) });
		expect_received({ 0x10, 0x20, 0x30, 0x40, 0x50, 0x60 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 3 });
	}
	BOOST_TEST_CONTEXT("Should accept Fragment 1/1")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(4), MessageSize(1), FragmentOffset(0), Bytes(0x30) });
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(4) });
		expect_received({ 0x30 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 4 });
	}
	BOOST_TEST_CONTEXT("Should accept empty Fragment")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(5), MessageSize(0), FragmentOffset(0) });
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(5) });
		expect_received({});
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 5 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_receive_invalid, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		accept_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore empty packet")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, {});
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore message packet without sequence number")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { Prefix(Message) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore message packet with invalid token")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { InvalidPrefix(Message), SequenceNumber(1), Bytes(0x10) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore packet of unknown type")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { Prefix(0xff), SequenceNumber(2) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_receive_invalid_first_fragment, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		accept_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore fragment with incomplete header")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(0) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore fragment with extra payload")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(1), FragmentOffset(0), Bytes(0x10, 0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should fail to accept fragment")
	{
		with(_fail_send, true, [this]
		{
			receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(0), FragmentOffset(0) });
		});
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_receive_invalid_next_fragment, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		accept_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore second fragment before first")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(4), FragmentOffset(1), Bytes(0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should accept first fragment")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(1), MessageSize(4), FragmentOffset(0), Bytes(0x10) });
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 1 });
	}
	BOOST_TEST_CONTEXT("Should ignore second fragment with different message size")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(2), MessageSize(3), FragmentOffset(1), Bytes(0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 1 });
	}
	BOOST_TEST_CONTEXT("Should ignore second fragment with bad offset")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(2), MessageSize(4), FragmentOffset(2), Bytes(0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 1 });
	}
	BOOST_TEST_CONTEXT("Should ignore second fragment with bad sequence number")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(3), MessageSize(4), FragmentOffset(1), Bytes(0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 1 });
	}
	BOOST_TEST_CONTEXT("Should accept second fragment")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(2), MessageSize(4), FragmentOffset(1), Bytes(0x20) });
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(2) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 2 });
	}
	BOOST_TEST_CONTEXT("Should ignore too long third fragment")
	{
		receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(3), MessageSize(4), FragmentOffset(2), Bytes(0x30, 0x40, 0x50) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 2 });
	}
	BOOST_TEST_CONTEXT("Should fail to accept third fragment")
	{
		with(_fail_send, true, [this]
		{
			receive_packet(Address{ 1 }, { Prefix(Fragment), SequenceNumber(3), MessageSize(4), FragmentOffset(2), Bytes(0x30, 0x40) });
		});
		expect_sent(Address{ 1 }, { Prefix(Ack), SequenceNumber(3) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}
