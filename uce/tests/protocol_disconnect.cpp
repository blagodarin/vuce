#include <boost/test/unit_test.hpp>

#include <uce/src/assert.h>
#include "helpers.h"
#include "protocol.h"

BOOST_FIXTURE_TEST_CASE(test_disconnect, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start disconnecting")
	{
		initiate_connection(Address{ 1 });
		protocol().disconnect();
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		receive_packet(Address{ 1 }, { Prefix(ByeAck) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should reconnect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_disconnect_accept, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		accept_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Bye with extra payload")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { Prefix(Bye), SequenceNumber(1), Bytes(0x00) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		receive_packet(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		expect_sent(Address{ 1 }, { Prefix(ByeAck) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should reconnect")
	{
		accept_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_disconnect_accept_invalid, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		accept_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Bye with invalid prefix")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { InvalidPrefix(Bye), SequenceNumber(1) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Bye from another address")
	{
		receive_packet(Address{ 2 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		receive_packet(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		expect_sent(Address{ 1 }, { Prefix(ByeAck) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_disconnect_bye_bye, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start disconnecting")
	{
		accept_connection(Address{ 1 });
		protocol().disconnect();
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		receive_packet(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		expect_sent(Address{ 1 }, { Prefix(ByeAck) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_disconnect_failure, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should disconnect on network failure")
	{
		initiate_connection(Address{ 1 });
		with(_fail_send, true, [this]
		{
			protocol().disconnect();
		});
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_disconnect_invalid, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start disconnecting")
	{
		initiate_connection(Address{ 1 });
		protocol().disconnect();
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore ByeAck with invalid prefix")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { InvalidPrefix(ByeAck) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore ByeAck from another address")
	{
		receive_packet(Address{ 2 }, { Prefix(ByeAck) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore ByeAck with extra payload")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { Prefix(ByeAck), Bytes(0x00) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		receive_packet(Address{ 1 }, { Prefix(ByeAck) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_disconnect_queuing, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send message #1")
	{
		BOOST_CHECK(send_message({ 0x10, 0x20 }));
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10, 0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should queue message #2")
	{
		BOOST_CHECK(send_message({ 0x30, 0x40 }));
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(1, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should queue disconnect")
	{
		protocol().disconnect();
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(1, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should fail to queue message #3")
	{
		BOOST_CHECK(!send_message({ 0x50, 0x60 }));
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(1, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should unqueue and send message #2")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(2), Bytes(0x30, 0x40) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 3 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should fail to queue message #4")
	{
		BOOST_CHECK(!send_message({ 0x70, 0x80 }));
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 3 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should send disconnect packet")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(2) });
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(3) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 4 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should accept disconnect acknowledgement")
	{
		receive_packet(Address{ 1 }, { Prefix(ByeAck) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_disconnect_queuing_failure, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		initiate_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should send message 1")
	{
		BOOST_CHECK(send_message({ 0x10, 0x20 }));
		expect_sent(Address{ 1 }, { Prefix(Message), SequenceNumber(1), Bytes(0x10, 0x20) });
		check_state(Connected, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should queue disconnect")
	{
		protocol().disconnect();
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
	BOOST_TEST_CONTEXT("Should fail to send disconnect packet")
	{
		with(_fail_send, true, [this]
		{
			receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		});
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(2) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
		BOOST_CHECK_EQUAL(0, protocol().sender().queue_size());
	}
}
