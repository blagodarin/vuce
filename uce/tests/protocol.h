#pragma once

#include <list>

#include <uce/endpoint.h>
#include <uce/src/packet.h>
#include <uce/src/protocol.h>

class ProtocolTestTransport
{
public:
	struct Address
	{
		int _value = 0;
		constexpr explicit Address(int value) noexcept : _value{ value } {}
		constexpr bool operator==(Address const& other) const noexcept { return _value == other._value; }
		constexpr bool operator!=(Address const& other) const noexcept { return !(*this == other); }
	};

	using SendCallback = std::function<bool(Address const&, uce::Buffer&&)>;

	explicit ProtocolTestTransport(SendCallback const&);

	bool send_packet(Address const&, uce::Bytes header, uce::Bytes payload);

private:
	SendCallback const _send_callback;
};

class TestProtocolHelper
{
public:
	std::uint32_t generate_nonce() noexcept { return _nonce; }

	void set_nonce(std::uint32_t value) noexcept { _nonce = value; }

private:
	std::uint32_t _nonce = 0;
};

using TestProtocol = uce::Protocol<ProtocolTestTransport, TestProtocolHelper>;

class ProtocolTestFixture : private uce::EndpointCallbacks
{
public:
	using Address = ProtocolTestTransport::Address;

	static constexpr auto
		Disconnected = uce::ProtocolState::Disconnected,
		Connecting = uce::ProtocolState::Connecting,
		Accepting = uce::ProtocolState::Accepting,
		Connected = uce::ProtocolState::Connected,
		Disconnecting = uce::ProtocolState::Disconnecting;

	struct AckNum
	{
		std::uint32_t const _value;
		constexpr explicit AckNum(std::uint32_t value) noexcept : _value{ value } {}
	};

	struct SeqNum
	{
		std::uint32_t const _value;
		constexpr explicit SeqNum(std::uint32_t value) noexcept : _value{ value } {}
	};

	ProtocolTestFixture();
	~ProtocolTestFixture() override;

	void check_state(uce::ProtocolState, std::optional<Address> const&, SeqNum const&, AckNum const&) const;
	void expect_connected();
	void expect_disconnected();
	void expect_received(std::vector<std::uint8_t> const&);
	void expect_sent(Address const&, std::vector<std::vector<std::uint8_t>> const&);
	TestProtocol& protocol() { return _protocol; }
	TestProtocol const& protocol() const { return _protocol; }
	void receive_packet(Address const&, std::vector<std::vector<std::uint8_t>> const&);
	bool send_message(std::vector<std::uint8_t> const&);

public:
	static constexpr auto
		Probe = uce::ProbePacket::Type,
		Accept = uce::AcceptPacket::Type,
		Confirm = uce::ConfirmPacket::Type,
		Bye = uce::ByePacket::Type,
		ByeAck = uce::ByeAckPacket::Type,
		Ack = uce::AckPacket::Type,
		Message = uce::MessagePacketHeader::Type,
		Fragment = uce::FragmentPacketHeader::Type;

	static std::vector<std::uint8_t> Prefix(std::uint8_t type) { return { 0xab, 0xcd, 0xef, type }; }
	static std::vector<std::uint8_t> InvalidPrefix(std::uint8_t type) { return { 0xfe, 0xdc, 0xba, type }; }
	static std::vector<std::uint8_t> Nonce(std::uint32_t value) { return make_uint32(value); }
	static std::vector<std::uint8_t> SequenceNumber(std::uint32_t value) { return make_uint32(value); }
	static std::vector<std::uint8_t> MessageSize(std::uint32_t value) { return make_uint32(value); }
	static std::vector<std::uint8_t> FragmentOffset(std::uint32_t value) { return make_uint32(value); }

	template <typename... Args>
	static std::vector<std::uint8_t> Bytes(Args&&... args)
	{
		return { static_cast<std::uint8_t>(std::forward<Args>(args))... };
	}

protected:
	bool _fail_send = false;

	void accept_connection(Address const&);
	void advance_time(std::chrono::milliseconds);
	void initiate_connection(Address const&);

private:
	void on_connected(std::shared_ptr<uce::Connection> const&) override;
	void on_disconnected(std::shared_ptr<uce::Connection> const&) override;
	void on_received(std::shared_ptr<uce::Connection> const&, uce::Buffer&&) override;

	bool send_packet(Address const&, uce::Buffer&&);

	static std::vector<std::uint8_t> make_uint32(std::uint32_t value)
	{
		return { static_cast<std::uint8_t>(value), static_cast<std::uint8_t>(value >> 8),
			static_cast<std::uint8_t>(value >> 16), static_cast<std::uint8_t>(value >> 24) };
	}

private:
	struct TrackItem;

	ProtocolTestTransport _transport{ [this](Address const& address, uce::Buffer&& buffer) { return send_packet(address, std::move(buffer)); } };
	TestProtocol _protocol;
	std::shared_ptr<uce::Connection> _connection;
	std::list<TrackItem> _track;
};

namespace uce
{
	std::ostream& operator<<(std::ostream&, ProtocolState);
}
