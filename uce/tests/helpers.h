#pragma once

#include <functional>
#include <iosfwd>

template <typename T>
void with(T& value, T&& temporary, std::function<void()> const& callback)
{
	class Holder
	{
	public:
		Holder(T& target, T&& new_value) : _target{ target }, _old_value{ std::move(_target) } { _target = std::forward<T>(new_value); }
		~Holder() noexcept { _target = std::move(_old_value); }
	private:
		T & _target;
		T _old_value;
	} holder{ value, std::forward<T>(temporary) };
	callback();
}

namespace uce
{
	std::ostream& operator<<(std::ostream&, class Buffer const&);
}
