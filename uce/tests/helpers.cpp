#include "helpers.h"

#include <array>
#include <iomanip>

#include <boost/test/unit_test.hpp>

#include <uce/buffer.h>

namespace uce
{
	std::ostream& operator<<(std::ostream& stream, Buffer const& buffer)
	{
		auto const print = [&stream](std::byte byte)
		{
			stream << std::setw(2) << std::setfill('0') << std::to_integer<unsigned>(byte);
		};

		stream << '{';
		if (!buffer.empty())
		{
			stream << std::hex;
			auto i = buffer.data();
			print(*i++);
			for (auto const end = buffer.data() + buffer.size(); i != end; ++i)
			{
				stream << ',';
				print(*i);
			}
			stream << std::dec;
		}
		stream << '}';
		return stream;
	}
}

BOOST_AUTO_TEST_CASE(test_helpers)
{
	std::array<std::uint8_t, 3> const bytes{ 0x10, 0x20, 0x30 };
	std::ostringstream stream;
	stream << uce::Buffer{ bytes.data(), bytes.size() };
	BOOST_CHECK_EQUAL("{10,20,30}", stream.str());
}
