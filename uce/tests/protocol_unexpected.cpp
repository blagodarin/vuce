// Tests for receiving unexpected valid control packets.

#include <boost/test/unit_test.hpp>

#include "helpers.h"
#include "protocol.h"

BOOST_FIXTURE_TEST_CASE(test_unexpected_disconnected, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should ignore Accept when disconnected")
	{
		receive_packet(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(1) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Confirm when disconnected")
	{
		receive_packet(Address{ 1 }, { Prefix(Confirm), Nonce(0) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Bye when disconnected")
	{
		receive_packet(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore ByeAck when disconnected")
	{
		receive_packet(Address{ 1 }, { Prefix(ByeAck) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Ack when disconnected")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_unexpected_connecting, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().set_remote_address(Address{ 1 });
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(0) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Confirm when connecting")
	{
		receive_packet(Address{ 1 }, { Prefix(Confirm), Nonce(0) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Bye when connecting")
	{
		receive_packet(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore ByeAck when connecting")
	{
		receive_packet(Address{ 1 }, { Prefix(ByeAck) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Ack when connecting")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_unexpected_accepting, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start accepting")
	{
		protocol().helper().set_nonce(1);
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(0) });
		expect_sent(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(1) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Probe when accepting")
	{
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(0) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Accept when accepting")
	{
		receive_packet(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(1) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Bye when accepting")
	{
		receive_packet(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore ByeAck when accepting")
	{
		receive_packet(Address{ 1 }, { Prefix(ByeAck) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Ack when accepting")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_unexpected_connected, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should connect")
	{
		accept_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Probe when connected")
	{
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(0) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Accept when connected")
	{
		receive_packet(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(1) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Confirm when connected")
	{
		receive_packet(Address{ 1 }, { Prefix(Confirm), Nonce(0) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore ByeAck when connected")
	{
		receive_packet(Address{ 1 }, { Prefix(ByeAck) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Ack when connected")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_unexpected_disconnecting, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start disconnecting")
	{
		accept_connection(Address{ 1 });
		protocol().disconnect();
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Probe when disconnecting")
	{
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(0) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Accept when disconnecting")
	{
		receive_packet(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Confirm when disconnecting")
	{
		receive_packet(Address{ 1 }, { Prefix(Confirm), Nonce(0) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Ack when disconnecting")
	{
		receive_packet(Address{ 1 }, { Prefix(Ack), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
}
