#include "protocol.h"

#include <variant>

#include <boost/test/unit_test.hpp>

#include "helpers.h"

namespace
{
	class ProtocolTestConnection final : public uce::Connection
	{
	public:
		bool close() override
		{
			throw std::logic_error{ "uce::Connection::close() should not be called in tests" };
		}

		bool send(uce::Buffer&&) override
		{
			throw std::logic_error{ "uce::Connection::send() should not be called in tests" };
		}
	};

	uce::Buffer merge(std::vector<std::vector<std::uint8_t>> const& parts)
	{
		std::vector<std::uint8_t> packet;
		for (auto const& part : parts)
			packet.insert(packet.end(), part.cbegin(), part.cend());
		return uce::Buffer{ packet.data(), packet.size() };
	}
}

ProtocolTestTransport::ProtocolTestTransport(SendCallback const& send_callback)
	: _send_callback{ send_callback }
{
}

bool ProtocolTestTransport::send_packet(Address const& address, uce::Bytes header, uce::Bytes payload)
{
	uce::Buffer packet{ nullptr, header.size() + payload.size() };
	std::memcpy(packet.data(), header.data(), header.size());
	if (!payload.empty())
		std::memcpy(packet.data() + header.size(), payload.data(), payload.size());
	return _send_callback(address, std::move(packet));
}

namespace
{
	struct OnConnected
	{
		std::shared_ptr<uce::Connection> _connection;

		OnConnected(std::shared_ptr<uce::Connection> const& connection)
			: _connection{ std::move(connection) }
		{
		}
	};

	struct OnDisconnected
	{
		std::shared_ptr<uce::Connection> _connection;

		OnDisconnected(std::shared_ptr<uce::Connection> const& connection)
			: _connection{ std::move(connection) }
		{
		}
	};

	struct OnReceived
	{
		std::shared_ptr<uce::Connection> _connection;
		uce::Buffer _message;

		OnReceived(std::shared_ptr<uce::Connection> const& connection, uce::Buffer&& message)
			: _connection{ std::move(connection) }
			, _message{ std::move(message) }
		{
		}
	};

	struct OnSent
	{
		ProtocolTestTransport::Address _address;
		uce::Buffer _packet;

		OnSent(ProtocolTestTransport::Address const& address, uce::Buffer&& packet)
			: _address{ address }
			, _packet{ std::move(packet) }
		{
		}
	};
}

struct ProtocolTestFixture::TrackItem
{
	std::variant<OnConnected, OnDisconnected, OnReceived, OnSent> _value;

	template <typename... Args>
	TrackItem(Args&&... args)
		: _value{ std::forward<Args>(args)... }
	{
	}

	template <typename T>
	T const* get() const
	{
		return std::get_if<T>(&_value);
	}

	std::string type_name() const
	{
		return std::visit([](auto&& value)
		{
			using T = std::decay_t<decltype(value)>;
			if constexpr (std::is_same_v<T, OnConnected>)
				return "OnConnected";
			else if constexpr (std::is_same_v<T, OnDisconnected>)
				return "OnDisconnected";
			else if constexpr (std::is_same_v<T, OnReceived>)
				return "OnReceived";
			else if constexpr (std::is_same_v<T, OnSent>)
				return "OnSent";
			else
				static_assert(std::is_same_v<T, void>);
		}, _value);
	}
};

ProtocolTestFixture::ProtocolTestFixture()
	: _protocol{ _transport, 0x00efcdab, *this, [](std::weak_ptr<TestProtocol>&&) { return std::make_unique<ProtocolTestConnection>(); } }
{
}

ProtocolTestFixture::~ProtocolTestFixture() = default;

void ProtocolTestFixture::check_state(uce::ProtocolState state, std::optional<Address> const& address, SeqNum const& sequence_number, AckNum const& acknowledge_number) const
{
	auto const name = !_track.empty() ? _track.front().type_name() : std::string{};
	BOOST_REQUIRE_MESSAGE(_track.empty(), "Expected nothing but got '" << name << "'");
	BOOST_CHECK_EQUAL(state, _protocol.state());
	BOOST_CHECK(address == _protocol.remote_address());
	BOOST_CHECK_EQUAL(sequence_number._value, _protocol.sender().sequence_number());
	BOOST_CHECK_EQUAL(acknowledge_number._value, _protocol.acknowledge_number());
}

void ProtocolTestFixture::expect_connected()
{
	BOOST_REQUIRE_MESSAGE(!_track.empty(), "Expected 'OnConnected' but got nothing");
	auto const item = _track.front().get<OnConnected>();
	BOOST_REQUIRE_MESSAGE(item, "Expected 'OnConnected' but got '" << _track.front().type_name() << "'");
	BOOST_CHECK(!_connection);
	BOOST_REQUIRE(item->_connection);
	BOOST_CHECK_THROW(item->_connection->send({}), std::logic_error);
	BOOST_CHECK_THROW(item->_connection->close(), std::logic_error);
	_connection = item->_connection;
	_track.pop_front();
}

void ProtocolTestFixture::expect_disconnected()
{
	BOOST_REQUIRE_MESSAGE(!_track.empty(), "Expected 'OnDisconnected' but got nothing");
	auto const item = _track.front().get<OnDisconnected>();
	BOOST_REQUIRE_MESSAGE(item, "Expected 'OnDisconnected' but got '" << _track.front().type_name() << "'");
	BOOST_CHECK_EQUAL(_connection, item->_connection);
	_connection.reset();
	_track.pop_front();
}

void ProtocolTestFixture::expect_received(std::vector<std::uint8_t> const& message)
{
	BOOST_REQUIRE_MESSAGE(!_track.empty(), "Expected 'OnReceived' but got nothing");
	auto const item = _track.front().get<OnReceived>();
	BOOST_REQUIRE_MESSAGE(item, "Expected 'OnReceived' but got '" << _track.front().type_name() << "'");
	BOOST_CHECK_EQUAL(_connection, item->_connection);
	BOOST_CHECK_EQUAL(uce::Buffer(message.data(), message.size()), item->_message);
	_track.pop_front();
}

void ProtocolTestFixture::expect_sent(Address const& address, std::vector<std::vector<std::uint8_t>> const& parts)
{
	BOOST_REQUIRE_MESSAGE(!_track.empty(), "Expected 'OnSent' but got nothing");
	auto const item = _track.front().get<OnSent>();
	BOOST_REQUIRE_MESSAGE(item, "Expected 'OnSent' but got '" << _track.front().type_name() << "'");
	BOOST_CHECK_EQUAL(address._value, item->_address._value);
	BOOST_CHECK_EQUAL(::merge(parts), item->_packet);
	_track.pop_front();
}

void ProtocolTestFixture::receive_packet(Address const& address, std::vector<std::vector<std::uint8_t>> const& parts)
{
	auto packet = ::merge(parts);
	_protocol.receive_packet(address, { reinterpret_cast<std::byte const*>(packet.data()), packet.size() });
}

bool ProtocolTestFixture::send_message(std::vector<std::uint8_t> const& message)
{
	return _protocol.send_message(uce::Buffer{ message.data(), message.size() });
}

void ProtocolTestFixture::accept_connection(Address const& address)
{
	_protocol.helper().set_nonce(3);
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		receive_packet(address, { Prefix(Probe), Nonce(7) });
		expect_sent(address, { Prefix(Accept), Nonce(7), Nonce(3) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should establish connection")
	{
		receive_packet(address, { Prefix(Confirm), Nonce(3) });
		expect_connected();
		check_state(Connected, address, SeqNum{ 1 }, AckNum{ 0 });
	}
}

void ProtocolTestFixture::advance_time(std::chrono::milliseconds duration)
{
	_protocol.set_time(_protocol.current_time() + duration);
}

void ProtocolTestFixture::initiate_connection(Address const& address)
{
	_protocol.helper().set_nonce(7);
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().set_remote_address(address);
		expect_sent(address, { Prefix(Probe), Nonce(7) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should establish connection")
	{
		receive_packet(address, { Prefix(Accept), Nonce(7), Nonce(3) });
		expect_sent(address, { Prefix(Confirm), Nonce(3) });
		expect_connected();
		check_state(Connected, address, SeqNum{ 1 }, AckNum{ 0 });
	}
}

void ProtocolTestFixture::on_connected(std::shared_ptr<uce::Connection> const& connection)
{
	BOOST_REQUIRE(connection);
	_track.emplace_back(std::in_place_type_t<OnConnected>{}, connection);
}

void ProtocolTestFixture::on_disconnected(std::shared_ptr<uce::Connection> const& connection)
{
	BOOST_REQUIRE(connection);
	_track.emplace_back(std::in_place_type_t<OnDisconnected>{}, connection);
}

void ProtocolTestFixture::on_received(std::shared_ptr<uce::Connection> const& connection, uce::Buffer&& buffer)
{
	BOOST_REQUIRE(connection);
	_track.emplace_back(std::in_place_type_t<OnReceived>{}, connection, std::move(buffer));
}

bool ProtocolTestFixture::send_packet(Address const& address, uce::Buffer&& packet)
{
	_track.emplace_back(std::in_place_type_t<OnSent>{}, address, std::move(packet));
	return !_fail_send;
}

namespace uce
{
	std::ostream& operator<<(std::ostream& stream, ProtocolState state)
	{
		switch (state)
		{
		case ProtocolState::Disconnected: stream << "Disconnected"; break;
		case ProtocolState::Connecting: stream << "Connecting"; break;
		case ProtocolState::Accepting: stream << "Accepting"; break;
		case ProtocolState::Connected: stream << "Connected"; break;
		case ProtocolState::Disconnecting: stream << "Disconnecting"; break;
		}
		return stream;
	}
}

BOOST_AUTO_TEST_CASE(test_protocol_helpers)
{
	auto const print = [](uce::ProtocolState state)
	{
		std::ostringstream stream;
		stream << state;
		return stream.str();
	};
	BOOST_CHECK_EQUAL("Disconnected", print(uce::ProtocolState::Disconnected));
	BOOST_CHECK_EQUAL("Connecting", print(uce::ProtocolState::Connecting));
	BOOST_CHECK_EQUAL("Accepting", print(uce::ProtocolState::Accepting));
	BOOST_CHECK_EQUAL("Connected", print(uce::ProtocolState::Connected));
	BOOST_CHECK_EQUAL("Disconnecting", print(uce::ProtocolState::Disconnecting));
}
