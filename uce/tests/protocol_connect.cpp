#include <boost/test/unit_test.hpp>

#include <uce/src/assert.h>
#include "helpers.h"
#include "protocol.h"

BOOST_FIXTURE_TEST_CASE(test_accept, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should fail to start connecting")
	{
		with(_fail_send, true, [this]
		{
			receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(0) });
		});
		expect_sent(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(0) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should connect")
	{
		accept_connection(Address{ 1 });
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_accept_invalid, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should ignore Probe without nonce")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { Prefix(Probe) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Probe with invalid prefix")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { InvalidPrefix(Probe), Nonce(0) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Probe with extra payload")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(0), Bytes(0x00) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should start accepting")
	{
		protocol().helper().set_nonce(1);
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(0) });
		expect_sent(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(1) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Confirm with invalid prefix")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { InvalidPrefix(Confirm), Nonce(0) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Confirm with invalid nonce")
	{
		receive_packet(Address{ 1 }, { Prefix(Confirm), Nonce(0) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Confirm with extra payload")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { Prefix(Confirm), Nonce(1), Bytes(0x00) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should connect")
	{
		receive_packet(Address{ 1 }, { Prefix(Confirm), Nonce(1) });
		expect_connected();
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_connect_failure, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should fail to start connecting")
	{
		with(_fail_send, true, [this]
		{
			protocol().set_remote_address(Address{ 1 });
		});
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(0) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().set_remote_address(Address{ 1 });
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(0) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should fail to establish connection")
	{
		with(_fail_send, true, [this]
		{
			receive_packet(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(1) });
		});
		expect_sent(Address{ 1 }, { Prefix(Confirm), Nonce(1) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should not establish connection after a failure")
	{
		receive_packet(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(1) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_connect_hello_hello_equal, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().helper().set_nonce(7);
		protocol().set_remote_address(Address{ 1 });
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(7) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Probe with equal nonce")
	{
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(7) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_connect_hello_hello_greater, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().helper().set_nonce(7);
		protocol().set_remote_address(Address{ 1 });
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(7) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should accept Probe with greater nonce")
	{
		protocol().helper().set_nonce(9);
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(8) });
		expect_sent(Address{ 1 }, { Prefix(Accept), Nonce(8), Nonce(9) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_connect_hello_hello_less, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().helper().set_nonce(7);
		protocol().set_remote_address(Address{ 1 });
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(7) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Probe with lesser nonce")
	{
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(6) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_connect_invalid, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().set_remote_address(Address{ 1 });
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(0) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Accept with invalid prefix")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { InvalidPrefix(Accept), Nonce(0), Nonce(1) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Accept with invalid nonce")
	{
		receive_packet(Address{ 1 }, { Prefix(Accept), Nonce(1), Nonce(0) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should ignore Accept with extra payload")
	{
		UCE_EXPECT_ASSERT;
		receive_packet(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(1), Bytes(0x00) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should establish connection")
	{
		receive_packet(Address{ 1 }, { Prefix(Accept), Nonce(0), Nonce(1) });
		expect_sent(Address{ 1 }, { Prefix(Confirm), Nonce(1) });
		expect_connected();
		check_state(Connected, Address{ 1 }, SeqNum{ 1 }, AckNum{ 0 });
	}
}
