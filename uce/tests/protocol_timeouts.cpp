#include <boost/test/unit_test.hpp>

#include "helpers.h"
#include "protocol.h"

using namespace std::literals::chrono_literals;

BOOST_FIXTURE_TEST_CASE(test_timeout_accepting, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start accepting")
	{
		protocol().helper().set_nonce(5);
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(3) });
		expect_sent(Address{ 1 }, { Prefix(Accept), Nonce(3), Nonce(5) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should stop accepting")
	{
		advance_time(uce::AcceptTimeout);
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_timeout_accepting_2, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start accepting")
	{
		protocol().helper().set_nonce(5);
		receive_packet(Address{ 1 }, { Prefix(Probe), Nonce(3) });
		expect_sent(Address{ 1 }, { Prefix(Accept), Nonce(3), Nonce(5) });
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should stop accepting")
	{
		advance_time(uce::AcceptTimeout - 1ms);
		check_state(Accepting, {}, SeqNum{ 0 }, AckNum{ 0 });
		advance_time(1ms);
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_timeout_connecting, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().helper().set_nonce(7);
		protocol().set_remote_address(Address{ 1 });
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(7) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should resend Probe")
	{
		protocol().helper().set_nonce(11);
		advance_time(uce::ProbeDelay);
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(11) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should resend Probe again")
	{
		protocol().helper().set_nonce(13);
		advance_time(uce::ProbeDelay);
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(13) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_timeout_connecting_2, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().helper().set_nonce(7);
		protocol().set_remote_address(Address{ 1 });
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(7) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should resend Probe")
	{
		protocol().helper().set_nonce(11);
		advance_time(uce::ProbeDelay - 1ms);
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
		advance_time(1ms);
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(11) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_timeout_connecting_failure, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start connecting")
	{
		protocol().helper().set_nonce(7);
		protocol().set_remote_address(Address{ 1 });
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(7) });
		check_state(Connecting, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should fail to resend Probe")
	{
		protocol().helper().set_nonce(11);
		with(_fail_send, true, [this]
		{
			advance_time(uce::ProbeDelay);
		});
		expect_sent(Address{ 1 }, { Prefix(Probe), Nonce(11) });
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_timeout_disconnecting, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start disconnecting")
	{
		initiate_connection(Address{ 1 });
		protocol().disconnect();
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		advance_time(uce::DisconnectTimeout);
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_timeout_disconnecting_2, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start disconnecting")
	{
		initiate_connection(Address{ 1 });
		protocol().disconnect();
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		advance_time(uce::DisconnectTimeout - 1ms);
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
		advance_time(1ms);
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_timeout_disconnecting_3, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start disconnecting")
	{
		initiate_connection(Address{ 1 });
		protocol().disconnect();
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		for (auto i = uce::DisconnectTimeout.count() / uce::DisconnectDelay.count(); i > 0; --i)
		{
			advance_time(uce::DisconnectDelay - 1ms);
			check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
			if (i > 1)
			{
				advance_time(1ms);
				expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
				check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
			}
		}
		advance_time(1ms);
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}

BOOST_FIXTURE_TEST_CASE(test_timeout_disconnecting_failure, ProtocolTestFixture)
{
	BOOST_TEST_CONTEXT("Should start disconnecting")
	{
		initiate_connection(Address{ 1 });
		protocol().disconnect();
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		check_state(Disconnecting, Address{ 1 }, SeqNum{ 2 }, AckNum{ 0 });
	}
	BOOST_TEST_CONTEXT("Should disconnect")
	{
		with(_fail_send, true, [this]
		{
			advance_time(uce::DisconnectDelay);
		});
		expect_sent(Address{ 1 }, { Prefix(Bye), SequenceNumber(1) });
		expect_disconnected();
		check_state(Disconnected, {}, SeqNum{ 0 }, AckNum{ 0 });
	}
}
