#pragma once

#include <cassert>

#ifndef NDEBUG

namespace uce
{
	extern bool _ignore_asserts;
	extern unsigned _failed_asserts;

	class ExpectAssert
	{
	public:
		ExpectAssert() noexcept : _old_ignore_asserts{ _ignore_asserts }, _old_failed_asserts{ _failed_asserts } { _ignore_asserts = true; }
		~ExpectAssert() noexcept { assert(_failed_asserts - _old_failed_asserts == 1); _ignore_asserts = _old_ignore_asserts; }
	private:
		bool const _old_ignore_asserts;
		unsigned const _old_failed_asserts;
	};
}

#define UCE_ASSERT(condition) do { if (uce::_ignore_asserts) ++uce::_failed_asserts; else assert(condition); } while (false)
#define UCE_EXPECT_ASSERT uce::ExpectAssert _uce_expect_assert

#else

#define UCE_ASSERT(condition) assert(condition)
#define UCE_EXPECT_ASSERT do {} while (false)

#endif
