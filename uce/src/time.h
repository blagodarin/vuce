#pragma once

#include <chrono>

namespace uce
{
	constexpr auto TimerResolution = std::chrono::milliseconds{ 20 };
	constexpr auto ProbeDelay = std::chrono::seconds{ 1 };
	constexpr auto AcceptTimeout = std::chrono::seconds{ 10 };
	constexpr auto DisconnectDelay = std::chrono::seconds{ 1 };
	constexpr auto DisconnectTimeout = std::chrono::seconds{ 10 };

	static_assert(0 == DisconnectTimeout.count() % DisconnectDelay.count());
}
