#pragma once

#include <cassert>
#include <deque>
#include <limits>
#include <optional>

#include "../buffer.h"
#include "packet.h"
#include "time.h"
#include "utils.h"

namespace uce
{
	template <class Transport>
	class Sender
	{
	public:
		using Address = typename Transport::Address;

		Sender(Transport&, std::uint32_t token);

		bool acknowledge(std::chrono::milliseconds timestamp, std::uint32_t sequence_number);
		bool disconnect(std::chrono::milliseconds timestamp);
		auto queue_size() const noexcept { return _outbound_queue.size(); }
		void reset();
		bool send_message(std::chrono::milliseconds timestamp, Buffer&&);
		bool send_packet(Address const&, Bytes) const;
		auto sequence_number() const noexcept { return _next_sequence; }
		void set_max_fragment_size(std::uint32_t size) noexcept { _max_fragment_size = size; }
		void set_remote_address(Address const& address) { _remote_address = address; }
		void set_sequence_number(std::uint32_t sequence_number) noexcept { _next_sequence = sequence_number; }
		void set_outbound_threshold(std::size_t bytes);
		bool update(std::chrono::milliseconds timestamp);

	private:
		bool send_outbound_buffer(std::chrono::milliseconds timestamp);

	private:
		Transport& _transport;
		std::uint32_t const _token;
		std::uint32_t _max_fragment_size = 1200;
		std::size_t _outbound_treshold = std::numeric_limits<std::size_t>::max();
		std::optional<Address> _remote_address;
		std::deque<Buffer> _outbound_queue;
		Buffer _outbound_buffer;
		std::size_t _total_data_size = 0;
		std::uint32_t _buffer_offset = 0;
		std::uint32_t _next_sequence = 0;
		bool _disconnecting = false;
		std::optional<std::chrono::milliseconds> _last_send_time;
	};

	template <class Transport>
	Sender<Transport>::Sender(Transport& transport, std::uint32_t token)
		: _transport{ transport }
		, _token{ token }
	{
	}

	template <class Transport>
	bool Sender<Transport>::acknowledge(std::chrono::milliseconds timestamp, std::uint32_t sequence_number)
	{
		if (sequence_number != _next_sequence - 1)
			return true;

		if (_outbound_buffer.empty())
		{
			assert(_outbound_queue.empty());
			return true;
		}

		if (_buffer_offset < _outbound_buffer.size())
		{
			// Outbound buffer partially acknowledged.
			auto const message_size = static_cast<std::uint32_t>(_outbound_buffer.size());
			auto const fragment_size = std::min(_max_fragment_size, message_size - _buffer_offset);
			FragmentPacketHeader const header{ _token, _next_sequence, message_size, _buffer_offset };
			if (!_transport.send_packet(*_remote_address, bytes_of(header), { _outbound_buffer.data() + _buffer_offset, fragment_size }))
				return false;
			_buffer_offset += _max_fragment_size;
			++_next_sequence;
			_last_send_time = timestamp;
		}
		else if (!_outbound_queue.empty())
		{
			// Outbound buffer acknowledged, have more data.
			_outbound_buffer = std::move(_outbound_queue.front());
			_outbound_queue.pop_front();
			_buffer_offset = 0;
			if (!send_outbound_buffer(timestamp))
				return false;
		}
		else
		{
			// Outbound buffer acknowledged, no more data.
			_outbound_buffer.reset();
			_total_data_size = 0;
			_buffer_offset = 0;
			if (_disconnecting)
			{
				assert(_remote_address);
				ByePacket const packet{ _token, _next_sequence };
				if (!_transport.send_packet(*_remote_address, bytes_of(packet), {}))
					return false;
				++_next_sequence;
				_last_send_time = timestamp;
			}
		}
		return true;
	}

	template <class Transport>
	bool Sender<Transport>::disconnect(std::chrono::milliseconds timestamp)
	{
		assert(_remote_address);
		_disconnecting = true;
		if (!_outbound_buffer.empty())
			return true;
		assert(_outbound_queue.empty());
		ByePacket const packet{ _token, _next_sequence };
		if (!_transport.send_packet(*_remote_address, bytes_of(packet), {}))
			return false;
		++_next_sequence;
		_last_send_time = timestamp;
		return true;
	}

	template <class Transport>
	void Sender<Transport>::reset()
	{
		_remote_address.reset();
		_outbound_queue.clear();
		_outbound_buffer.reset();
		_total_data_size = 0;
		_buffer_offset = 0;
		_next_sequence = 0;
		_disconnecting = false;
		_last_send_time.reset();
	}

	template <class Transport>
	bool Sender<Transport>::send_message(std::chrono::milliseconds timestamp, Buffer&& buffer)
	{
		assert(!_disconnecting);
		if (buffer.empty())
			return false;
		if constexpr (sizeof(std::size_t) > sizeof(std::uint32_t))
			if (buffer.size() > std::numeric_limits<std::uint32_t>::max())
				return false;
		if (_outbound_buffer.empty())
		{
			assert(_outbound_queue.empty());
			assert(!_total_data_size);
			_outbound_buffer = std::move(buffer);
			if (!send_outbound_buffer(timestamp))
			{
				// TODO: Handle the situation properly.
				_outbound_buffer.reset();
				return true;
			}
			_total_data_size = _outbound_buffer.size();
			return true;
		}
		if (_total_data_size > _outbound_treshold)
			return false;
		while (_outbound_treshold - _total_data_size < buffer.size())
		{
			if (_outbound_queue.empty())
				return false;
			_total_data_size -= _outbound_queue.front().size();
			_outbound_queue.pop_front();
		}
		_total_data_size += buffer.size();
		_outbound_queue.emplace_back(std::move(buffer));
		return true;
	}

	template <class Transport>
	bool Sender<Transport>::send_packet(Address const& address, Bytes packet) const
	{
		return _transport.send_packet(address, packet, {});
	}

	template <class Transport>
	void Sender<Transport>::set_outbound_threshold(std::size_t bytes)
	{
		_outbound_treshold = bytes;
		while (_total_data_size > _outbound_treshold)
		{
			assert(_total_data_size >= _outbound_queue.front().size());
			_total_data_size -= _outbound_queue.front().size();
			_outbound_queue.pop_front();
		}
	}

	template <class Transport>
	bool Sender<Transport>::send_outbound_buffer(std::chrono::milliseconds timestamp)
	{
		assert(_remote_address);
		assert(!_outbound_buffer.empty());
		assert(_outbound_buffer.size() <= std::numeric_limits<std::uint32_t>::max());
		auto const data = _outbound_buffer.data();
		auto const size = static_cast<std::uint32_t>(_outbound_buffer.size());
		if (size <= _max_fragment_size)
		{
			MessagePacketHeader const header{ _token, _next_sequence };
			if (!_transport.send_packet(*_remote_address, bytes_of(header), { data, size }))
				return false;
			_buffer_offset = static_cast<std::uint32_t>(_outbound_buffer.size());
		}
		else
		{
			FragmentPacketHeader const header{ _token, _next_sequence, size, 0 };
			if (!_transport.send_packet(*_remote_address, bytes_of(header), { data, _max_fragment_size }))
				return false;
			_buffer_offset = _max_fragment_size;
		}
		++_next_sequence;
		_last_send_time = timestamp;
		return true;
	}

	template <class Transport>
	bool Sender<Transport>::update(std::chrono::milliseconds timestamp)
	{
		if (_outbound_buffer.empty())
		{
			assert(_outbound_queue.empty());
			if (_disconnecting)
			{
				assert(_last_send_time);
				assert(timestamp > *_last_send_time);
				if (timestamp - *_last_send_time >= DisconnectDelay)
				{
					ByePacket const packet{ _token, _next_sequence - 1 };
					if (!_transport.send_packet(*_remote_address, bytes_of(packet), {}))
						return false;
					_last_send_time = timestamp;
				}
			}
			return true;
		}
		assert(false); // TODO: Implement resending.
		return false;
	}
}
