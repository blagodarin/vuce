#include "../buffer.h"

#include <cstdlib>
#include <cstring>
#include <new>
#include <utility>

namespace uce
{
	Buffer::Buffer(Buffer&& buffer) noexcept
		: _data{ std::exchange(buffer._data, nullptr) }
		, _capacity{ std::exchange(buffer._capacity, 0) }
		, _size{ std::exchange(buffer._size, 0) }
	{
	}

	Buffer::~Buffer() noexcept
	{
		if (_capacity > 0)
			std::free(_data);
	}

	Buffer& Buffer::operator=(Buffer&& buffer) noexcept
	{
		if (this != &buffer)
		{
			if (_capacity > 0)
				std::free(_data);
			_data = std::exchange(buffer._data, nullptr);
			_capacity = std::exchange(buffer._capacity, 0);
			_size = std::exchange(buffer._size, 0);
		}
		return *this;
	}

	Buffer::Buffer(const void* data, std::size_t size)
		: _data{ nullptr }
		, _capacity{ size }
		, _size{ size }
	{
		if (size > 0)
		{
			_data = static_cast<std::byte*>(std::malloc(size));
			if (!_data)
				throw std::bad_alloc{};
			if (data)
				std::memcpy(_data, data, size);
		}
	}

	Buffer Buffer::by_reference(void* data, std::size_t size) noexcept
	{
		Buffer buffer;
		buffer._data = static_cast<std::byte*>(data);
		buffer._size = size;
		return buffer;
	}

	void Buffer::reset() noexcept
	{
		if (_capacity > 0)
			std::free(_data);
		_data = nullptr;
		_capacity = 0;
		_size = 0;
	}

	void Buffer::reset(std::size_t size)
	{
		if (_capacity == 0)
		{
			_data = nullptr;
			_size = 0;
		}
		if (size > _capacity)
		{
			std::free(_data);
			_data = static_cast<std::byte*>(std::malloc(size));
			if (!_data)
			{
				_capacity = 0;
				_size = 0;
				throw std::bad_alloc{};
			}
			_capacity = size;
		}
		_size = size;
	}

	bool operator==(const Buffer& a, const Buffer& b) noexcept
	{
		return a.size() == b.size() && !std::memcmp(a.data(), b.data(), a.size());
	}
}
