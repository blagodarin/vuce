#pragma once

#include <cstdint>

#include "../buffer.h"
#include "utils.h"

namespace uce
{
	struct FragmentPacketHeader;

	class Reassembler
	{
	public:
		bool push_fragment(FragmentPacketHeader const&, Bytes);
		bool has_message() const noexcept { return _buffer_offset == _buffer.size(); }
		void reset() noexcept;
		Buffer take_message() noexcept;

	private:
		bool push_first_fragment(FragmentPacketHeader const&, Bytes);
		bool push_next_fragment(FragmentPacketHeader const&, Bytes) noexcept;

	private:
		Buffer _buffer;
		std::size_t _buffer_offset = 0;
		std::uint32_t _next_sequence = 0;
	};
}
