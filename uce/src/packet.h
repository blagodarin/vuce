#pragma once

#include <cstdint>

namespace uce
{
	struct PacketPrefix
	{
		std::uint32_t _prefix = 0;

		constexpr PacketPrefix(std::uint32_t token, std::uint8_t type) noexcept
			: _prefix{ (token & 0x00ffffff) | std::uint32_t{ type } << 24 } {}

		constexpr std::uint32_t token() const noexcept { return _prefix & 0x00ffffff; }
		constexpr std::uint8_t type() const noexcept { return static_cast<std::uint8_t>(_prefix >> 24); }
	};
	static_assert(sizeof(PacketPrefix) == 4);

	template <std::uint8_t value>
	struct PacketType { static constexpr std::uint8_t Type = value; };

	struct ProbePacket : PacketPrefix, PacketType<0x00>
	{
		std::uint32_t _accept_nonce = 0;

		constexpr ProbePacket(std::uint32_t token, std::uint32_t accept_nonce) noexcept
			: PacketPrefix{ token, Type }, _accept_nonce{ accept_nonce } {}
	};
	static_assert(sizeof(ProbePacket) == 8);

	struct AcceptPacket : PacketPrefix, PacketType<0x01>
	{
		std::uint32_t _accept_nonce = 0;
		std::uint32_t _confirm_nonce = 0;

		constexpr AcceptPacket(std::uint32_t token, std::uint32_t accept_nonce, std::uint32_t confirm_nonce) noexcept
			: PacketPrefix{ token, Type }, _accept_nonce{ accept_nonce }, _confirm_nonce{ confirm_nonce } {}
	};
	static_assert(sizeof(AcceptPacket) == 12);

	struct ConfirmPacket : PacketPrefix, PacketType<0x02>
	{
		std::uint32_t _confirm_nonce = 0;

		constexpr ConfirmPacket(std::uint32_t token, std::uint32_t confirm_nonce) noexcept
			: PacketPrefix{ token, Type }, _confirm_nonce{ confirm_nonce } {}
	};
	static_assert(sizeof(ConfirmPacket) == 8);

	struct ByePacket : PacketPrefix, PacketType<0x03>
	{
		std::uint32_t _sequence_number = 0;

		constexpr ByePacket(std::uint32_t token, std::uint32_t sequence_number) noexcept
			: PacketPrefix{ token, Type }, _sequence_number{ sequence_number } {}
	};
	static_assert(sizeof(ByePacket) == 8);

	struct ByeAckPacket : PacketPrefix, PacketType<0x04>
	{
		constexpr explicit ByeAckPacket(std::uint32_t token) noexcept
			: PacketPrefix{ token, Type } {}
	};
	static_assert(sizeof(ByeAckPacket) == 4);

	struct AckPacket : PacketPrefix, PacketType<0x05>
	{
		std::uint32_t _sequence_number = 0;

		constexpr AckPacket(std::uint32_t token, std::uint32_t sequence_number) noexcept
			: PacketPrefix{ token, Type }, _sequence_number{ sequence_number } {}
	};
	static_assert(sizeof(AckPacket) == 8);

	struct MessagePacketHeader : PacketPrefix, PacketType<0x06>
	{
		std::uint32_t _sequence_number = 0;

		constexpr MessagePacketHeader(std::uint32_t token, std::uint32_t sequence_number) noexcept
			: PacketPrefix{ token, Type }, _sequence_number{ sequence_number } {}
	};
	static_assert(sizeof(MessagePacketHeader) == 8);

	struct FragmentPacketHeader : PacketPrefix, PacketType<0x07>
	{
		std::uint32_t _sequence_number = 0;
		std::uint32_t _message_size = 0;
		std::uint32_t _fragment_offset = 0;

		constexpr FragmentPacketHeader(std::uint32_t token, std::uint32_t sequence_number, std::uint32_t message_size, std::uint32_t fragment_offset) noexcept
			: PacketPrefix{ token, Type }, _sequence_number{ sequence_number }, _message_size{ message_size }, _fragment_offset{ fragment_offset } {}
	};
	static_assert(sizeof(FragmentPacketHeader) == 16);

	// TODO: Add message aggregation support.
}
