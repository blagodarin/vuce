#pragma once

#include <functional>
#include <random>

#include "../endpoint.h"
#include "assert.h"
#include "reassembler.h"
#include "sender.h"

namespace uce
{
	enum class ProtocolState
	{
		Disconnected,
		Connecting,    // Sent Probe, expecting Accept.
		Accepting,     // Received Probe, sent Accept, expecting Confirm.
		Connected,
		Disconnecting, // Sent Bye, expecting ByeAck.
	};

	class ProtocolHelper
	{
	public:
		std::uint32_t generate_nonce() noexcept { return _distribution(_generator); }

	private:
		std::random_device _device;
		std::mt19937 _generator{ _device() };
		std::uniform_int_distribution<std::uint32_t> _distribution;
	};

	template <class Transport, class Helper = ProtocolHelper>
	class Protocol
	{
	public:
		using Address = typename Transport::Address;
		using ConnectionFactory = std::function<std::unique_ptr<Connection>(std::weak_ptr<Protocol>&&)>;

		Protocol(Transport&, std::uint32_t token, EndpointCallbacks&, ConnectionFactory const&);
		~Protocol() noexcept;

		void disconnect();
		void receive_packet(Address const&, Bytes);
		bool send_message(Buffer&& buffer) { return _session && _sender.send_message(_current_time, std::move(buffer)); }
		auto& sender() { return _sender; }
		auto& sender() const { return _sender; }
		void set_remote_address(Address const&);
		void set_time(std::chrono::milliseconds);
		bool stop();

	public: // For testing.
		auto acknowledge_number() const noexcept { return _acknowledge_number; }
		auto current_time() const noexcept { return _current_time; }
		Helper& helper() noexcept { return _helper; }
		std::optional<Address> remote_address() const { return _connection_address; }
		auto state() const noexcept { return _state; }

	private:
		void receive_probe(Address const&, struct ProbePacket const&);
		void receive_accept(Address const&, struct AcceptPacket const&);
		void receive_confirm(Address const&, struct ConfirmPacket const&);
		void receive_bye(Address const&, struct ByePacket const&);
		void receive_bye_ack(Address const&, struct ByeAckPacket const&);
		void receive_ack(Address const&, struct AckPacket const&);
		void receive_message(Address const&, struct MessagePacketHeader const&, Bytes);
		void receive_fragment(Address const&, struct FragmentPacketHeader const&, Bytes);

		void create_connection();
		void reset_connection();
		bool send_ack(Address const&, std::uint32_t);
		void set_state(ProtocolState) noexcept;

	private:
		struct Session
		{
			Address _remote_address;
			std::uint32_t _nonce = 0;

			explicit Session(Address const& address, std::uint32_t nonce) : _remote_address{ address }, _nonce{ nonce } {}
		};

		std::uint32_t const _token;
		EndpointCallbacks& _callbacks;
		ConnectionFactory const _create_connection;
		Helper _helper;
		std::chrono::milliseconds _current_time{ 0 };
		ProtocolState _state = ProtocolState::Disconnected;
		std::chrono::milliseconds _state_time{ 0 };
		Sender<Transport> _sender;
		std::shared_ptr<Session> _session;
		std::shared_ptr<Connection> _connection;
		std::optional<Address> _connection_address;
		std::uint32_t _acknowledge_number = 0;
		Reassembler _reassembler;
	};

	template <class T, class H>
	Protocol<T, H>::Protocol(T& transport, std::uint32_t token, EndpointCallbacks& callbacks, ConnectionFactory const& connection_factory)
		: _token{ token & 0xffffff }
		, _callbacks{ callbacks }
		, _create_connection{ connection_factory }
		, _sender{ transport, _token }
	{
	}

	template <class T, class H>
	Protocol<T, H>::~Protocol() noexcept = default;

	template <class T, class H>
	void Protocol<T, H>::disconnect()
	{
		assert(_state == ProtocolState::Connected);
		assert(_session && _connection && _connection_address);
		if (!_sender.disconnect(_current_time))
		{
			reset_connection(); // TODO: Invoke 'on_disconnected' asynchronously.
			return;
		}
		_session.reset();
		set_state(ProtocolState::Disconnecting);
	}

	template <class T, class H>
	void Protocol<T, H>::set_time(std::chrono::milliseconds time)
	{
		assert(time >= _current_time);
		if (time > _current_time)
		{
			_current_time = time;
			auto const time_in_state = _current_time - _state_time;
			switch (_state)
			{
			case ProtocolState::Disconnected:
				break;

			case ProtocolState::Connecting:
				if (time_in_state >= ProbeDelay)
				{
					assert(_session);
					ProbePacket const packet{ _token, _helper.generate_nonce() };
					if (!_sender.send_packet(_session->_remote_address, bytes_of(packet)))
					{
						reset_connection();
						break;
					}
					_session->_nonce = packet._accept_nonce;
					_state_time = _current_time;
				}
				break;

			case ProtocolState::Accepting:
				if (time_in_state >= AcceptTimeout)
					reset_connection();
				break;

			case ProtocolState::Connected:
				if (!_sender.update(_current_time))
					reset_connection();
				break;

			case ProtocolState::Disconnecting:
				if (time_in_state >= DisconnectTimeout || !_sender.update(_current_time))
					reset_connection();
				break;
			}
		}
	}

	template <class T, class H>
	void Protocol<T, H>::receive_packet(Address const& address, Bytes packet)
	{
		if (packet.size() < sizeof(PacketPrefix))
		{
			UCE_ASSERT(false);
			return;
		}

		auto const& prefix = packet.cast<PacketPrefix>();
		if (prefix.token() != _token)
		{
			UCE_ASSERT(false);
			return;
		}

		switch (prefix.type())
		{
		case ProbePacket::Type:
			if (packet.size() != sizeof(ProbePacket))
			{
				UCE_ASSERT(false);
				break;
			}
			receive_probe(address, packet.cast<ProbePacket>());
			break;

		case AcceptPacket::Type:
			if (packet.size() != sizeof(AcceptPacket))
			{
				UCE_ASSERT(false);
				break;
			}
			receive_accept(address, packet.cast<AcceptPacket>());
			break;

		case ConfirmPacket::Type:
			if (packet.size() != sizeof(ConfirmPacket))
			{
				UCE_ASSERT(false);
				break;
			}
			receive_confirm(address, packet.cast<ConfirmPacket>());
			break;

		case ByePacket::Type:
			if (packet.size() != sizeof(ByePacket))
			{
				UCE_ASSERT(false);
				break;
			}
			receive_bye(address, packet.cast<ByePacket>());
			break;

		case ByeAckPacket::Type:
			if (packet.size() != sizeof(ByeAckPacket))
			{
				UCE_ASSERT(false);
				break;
			}
			receive_bye_ack(address, packet.cast<ByeAckPacket>());
			break;

		case AckPacket::Type:
			if (packet.size() != sizeof(AckPacket))
			{
				UCE_ASSERT(false);
				break;
			}
			receive_ack(address, packet.cast<AckPacket>());
			break;

		case MessagePacketHeader::Type:
			if (packet.size() < sizeof(MessagePacketHeader))
			{
				UCE_ASSERT(false);
				break;
			}
			receive_message(address, packet.cast<MessagePacketHeader>(), packet.from(sizeof(MessagePacketHeader)));
			break;

		case FragmentPacketHeader::Type:
			if (packet.size() < sizeof(FragmentPacketHeader))
			{
				UCE_ASSERT(false);
				break;
			}
			receive_fragment(address, packet.cast<FragmentPacketHeader>(), packet.from(sizeof(FragmentPacketHeader)));
			break;

		default:
			UCE_ASSERT(false);
		}
	}

	template <class T, class H>
	void Protocol<T, H>::set_remote_address(Address const& address)
	{
		if (_state != ProtocolState::Disconnected)
			return; // TODO: Start address change negotiation.
		assert(!_session && !_connection && !_connection_address);
		ProbePacket const packet{ _token, _helper.generate_nonce() };
		if (!_sender.send_packet(address, bytes_of(packet)))
			return;
		_session = std::make_shared<Session>(address, packet._accept_nonce);
		set_state(ProtocolState::Connecting);
	}

	template <class T, class H>
	bool Protocol<T, H>::stop()
	{
		switch (_state)
		{
		case ProtocolState::Disconnected:
			break;

		case ProtocolState::Connecting:
			reset_connection();
			break;

		case ProtocolState::Accepting:
			{
				ByePacket const packet{ _token, 1 };
				_sender.send_packet(_session->_remote_address, bytes_of(packet));
			}
			reset_connection();
			break;

		case ProtocolState::Connected:
			if (_sender.disconnect(_current_time))
			{
				_session.reset();
				set_state(ProtocolState::Disconnecting);
				return false;
			}
			reset_connection();
			break;

		case ProtocolState::Disconnecting:
			return false;
		}
		return true;
	}

	template <class T, class H>
	void Protocol<T, H>::receive_probe(Address const& address, ProbePacket const& packet)
	{
		switch (_state)
		{
		case ProtocolState::Disconnected:
			{
				auto session = std::make_shared<Session>(address, _helper.generate_nonce());
				AcceptPacket const response{ _token, packet._accept_nonce, session->_nonce };
				if (_sender.send_packet(address, bytes_of(response)))
				{
					_session = std::move(session);
					set_state(ProtocolState::Accepting);
				}
			}
			break;

		case ProtocolState::Connecting:
			if (_session->_remote_address == address && _session->_nonce < packet._accept_nonce)
			{
				AcceptPacket const response{ _token, packet._accept_nonce, _helper.generate_nonce() };
				if (_sender.send_packet(address, bytes_of(response)))
				{
					_session->_nonce = response._confirm_nonce;
					set_state(ProtocolState::Accepting);
				}
			}
			break;

		case ProtocolState::Accepting:
		case ProtocolState::Connected:
		case ProtocolState::Disconnecting:
			break;
		}
	}

	template <class T, class H>
	void Protocol<T, H>::receive_accept(Address const& address, AcceptPacket const& packet)
	{
		if (_state != ProtocolState::Connecting)
			return;
		if (_session->_remote_address != address || _session->_nonce != packet._accept_nonce)
			return;
		ConfirmPacket const response{ _token, packet._confirm_nonce };
		if (!_sender.send_packet(address, bytes_of(response)))
		{
			reset_connection();
			return;
		}
		create_connection();
	}

	template <class T, class H>
	void Protocol<T, H>::receive_confirm(Address const& address, struct ConfirmPacket const& packet)
	{
		if (_state != ProtocolState::Accepting)
			return;
		assert(_session && !_connection && !_connection_address);
		if (_session->_remote_address != address || _session->_nonce != packet._confirm_nonce)
			return;
		create_connection();
	}

	template <class T, class H>
	void Protocol<T, H>::receive_bye(Address const& address, struct ByePacket const&)
	{
		if (_state != ProtocolState::Connected && _state != ProtocolState::Disconnecting)
			return;
		assert(_connection);
		if (_connection_address != address)
			return;
		ByeAckPacket bye_ack{ _token };
		_sender.send_packet(address, bytes_of(bye_ack));
		reset_connection();
	}

	template <class T, class H>
	void Protocol<T, H>::receive_bye_ack(Address const& address, struct ByeAckPacket const&)
	{
		if (_state != ProtocolState::Disconnecting)
			return;
		assert(!_session && _connection);
		if (_connection_address != address)
			return;
		reset_connection();
	}

	template <class T, class H>
	void Protocol<T, H>::receive_ack(Address const& address, AckPacket const& packet)
	{
		if (_state != ProtocolState::Connected && _state != ProtocolState::Disconnecting)
			return;
		assert(_connection && _connection_address);
		if (_connection_address != address)
			return;
		if (!_sender.acknowledge(_current_time, packet._sequence_number))
			reset_connection();
	}

	template <class T, class H>
	void Protocol<T, H>::receive_message(Address const& address, MessagePacketHeader const& header, Bytes payload)
	{
		if (_state != ProtocolState::Connected && _state != ProtocolState::Disconnecting)
			return;
		assert(_connection);
		if (_connection_address != address)
			return;
		if (header._sequence_number > _acknowledge_number)
		{
			if (!send_ack(address, header._sequence_number))
			{
				reset_connection();
				return;
			}
			_callbacks.on_received(_connection, Buffer{ payload.data(), payload.size() });
		}
	}

	template <class T, class H>
	void Protocol<T, H>::receive_fragment(Address const& address, FragmentPacketHeader const& header, Bytes payload)
	{
		if (_state != ProtocolState::Connected && _state != ProtocolState::Disconnecting)
			return;
		assert(_connection);
		if (_connection_address != address)
			return;
		if (header._sequence_number > _acknowledge_number)
		{
			if (!_reassembler.push_fragment(header, payload))
				return;
			if (!send_ack(address, header._sequence_number))
			{
				reset_connection();
				return;
			}
			if (_reassembler.has_message())
				_callbacks.on_received(_connection, _reassembler.take_message());
		}
	}

	template <class T, class H>
	void Protocol<T, H>::create_connection()
	{
		_sender.set_remote_address(_session->_remote_address);
		_sender.set_sequence_number(1);
		_connection = _create_connection(std::shared_ptr<Protocol>(_session, this));
		_connection_address = _session->_remote_address;
		set_state(ProtocolState::Connected);
		_callbacks.on_connected(_connection);
	}

	template <class T, class H>
	void Protocol<T, H>::reset_connection()
	{
		_sender.reset();
		_session.reset();
		auto const connection = std::exchange(_connection, nullptr);
		_connection_address.reset();
		_acknowledge_number = 0;
		_reassembler.reset();
		set_state(ProtocolState::Disconnected);
		if (connection)
			_callbacks.on_disconnected(connection);
	}

	template <class T, class H>
	bool Protocol<T, H>::send_ack(Address const& address, std::uint32_t sequence_number)
	{
		AckPacket const packet{ _token, sequence_number };
		if (!_sender.send_packet(address, bytes_of(packet)))
			return false;
		_acknowledge_number = sequence_number;
		return true;
	}

	template <class T, class H>
	void Protocol<T, H>::set_state(ProtocolState state) noexcept
	{
		_state = state;
		_state_time = _current_time;
#ifndef NDEBUG
		switch (_state)
		{
		case ProtocolState::Disconnected:
			assert(!_session && !_connection && !_connection_address);
			break;

		case ProtocolState::Connecting:
			assert(_session && !_connection && !_connection_address);
			break;

		case ProtocolState::Accepting:
			assert(_session && !_connection && !_connection_address);
			break;

		case ProtocolState::Connected:
			assert(_session && _connection && _connection_address);
			break;

		case ProtocolState::Disconnecting:
			assert(!_session && _connection && _connection_address);
			break;
		}
#endif
	}
}
