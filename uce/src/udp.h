#pragma once

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>

#include "utils.h"

namespace uce
{
	class UdpTransport
	{
	public:
		using Address = boost::asio::ip::udp::endpoint;
		using ReceiveCallback = std::function<void(Address const&, Bytes)>;

		UdpTransport(std::uint16_t port, boost::asio::io_service&, ReceiveCallback const&);
		~UdpTransport() noexcept;

		bool send_packet(Address const&, Bytes header, Bytes payload);

	private:
		void receive();

	private:
		boost::asio::ip::udp::socket _socket;
		ReceiveCallback const _receive_callback;
		Address _remote_address;
		std::array<std::byte, 65536> _buffer;
	};
}
