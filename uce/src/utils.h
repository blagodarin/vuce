#pragma once

#include <cstddef>

namespace uce
{
	class Bytes
	{
	public:
		constexpr Bytes() noexcept = default;
		constexpr Bytes(std::byte const* data, std::size_t size) noexcept : _data{ data }, _size{ size } {}

		constexpr std::byte const* data() const noexcept { return _data; }
		constexpr bool empty() const noexcept { return !_size; }
		constexpr Bytes from(std::size_t offset) const noexcept { return { _data + offset, _size - offset }; }
		constexpr std::size_t size() const noexcept { return _size; }

		template <typename T>
		constexpr T const& cast() const noexcept { return *reinterpret_cast<T const*>(_data); }

	private:
		std::byte const* _data = nullptr;
		std::size_t _size = 0;
	};

	template <typename T>
	constexpr Bytes bytes_of(const T& value) noexcept
	{
		return { reinterpret_cast<std::byte const*>(&value), sizeof value };
	}
}
