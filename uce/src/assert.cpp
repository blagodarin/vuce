#include "assert.h"

#ifndef NDEBUG

namespace uce
{
	bool _ignore_asserts = false;
	unsigned _failed_asserts = 0;
}

#endif
