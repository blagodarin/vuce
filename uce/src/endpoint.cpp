#include "../endpoint.h"

#include <cassert>
#include <mutex>
#include <thread>

#include <boost/asio/io_service.hpp>
#include <boost/asio/steady_timer.hpp>

#include "protocol.h"
#include "udp.h"

namespace
{
	auto steady_clock_time() noexcept
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch());
	}
}

namespace uce
{
	using UdpProtocol = Protocol<UdpTransport>;

	class ConnectionImpl final : public Connection
	{
	public:
		ConnectionImpl(std::shared_ptr<std::recursive_mutex> const& mutex, std::weak_ptr<UdpProtocol>&& protocol)
			: _mutex{ mutex }
			, _protocol{ std::move(protocol) }
		{
		}

		bool close() override
		{
			std::scoped_lock lock{ *_mutex };
			if (auto const protocol = _protocol.lock())
			{
				protocol->disconnect();
				return true;
			}
			return false;
		}

		bool send(Buffer&& buffer) override
		{
			std::scoped_lock lock{ *_mutex };
			if (auto const protocol = _protocol.lock())
				return protocol->send_message(std::move(buffer));
			return false;
		}

	private:
		std::shared_ptr<std::recursive_mutex> const _mutex;
		std::weak_ptr<UdpProtocol> const _protocol;
	};

	class EndpointImpl final : public Endpoint
	{
	public:
		EndpointImpl(std::uint32_t token, std::uint16_t port, EndpointCallbacks& callbacks)
			: _callbacks(callbacks)
			, _transport(port, _io, [this](auto const& address, uce::Bytes packet) { receive(address, packet); })
			, _protocol(_transport, token, _callbacks, [this](std::weak_ptr<UdpProtocol>&& protocol) { return std::make_unique<ConnectionImpl>(_protocol_mutex, std::move(protocol)); })
			, _thread([this] { run(); })
		{
		}

		~EndpointImpl() noexcept override
		{
			assert(_thread.joinable());
			assert(_thread.get_id() != std::this_thread::get_id());
			_io.stop();
			_thread.join();
		}

		void set_outbound_threshold(std::size_t bytes) override
		{
			_io.post([this, bytes]
			{
				std::scoped_lock lock{ *_protocol_mutex };
				_protocol.sender().set_outbound_threshold(bytes);
			});
		}

		void start() override
		{
			_io.post([this]
			{
				assert(State::Stopped == _state || State::Started == _state);
				_state = State::Started;
			});
		}

		void start(UdpAddress const& address) override
		{
			_io.post([this, address]
			{
				assert(State::Stopped == _state || State::Started == _state);
				auto const previous_state = std::exchange(_state, State::Started);
				std::scoped_lock lock{ *_protocol_mutex };
				if (State::Stopped == previous_state)
				{
					assert(ProtocolState::Disconnected == _protocol.state());
					_protocol.set_time(::steady_clock_time());
				}
				_protocol.set_remote_address({ boost::asio::ip::address_v4{ { address._ip[0], address._ip[1], address._ip[2], address._ip[3] } }, address._port });
			});
		}

		void stop() override
		{
			_io.post([this]
			{
				if (State::Started == _state)
				{
					std::scoped_lock lock{ *_protocol_mutex };
					_state = _protocol.stop() ? State::Stopped : State::Stopping;
				}
				else
					assert(State::Stopped == _state || State::Stopping == _state);
			});
		}

	private:
		void receive(UdpTransport::Address const& address, uce::Bytes packet)
		{
			if (State::Started == _state || State::Stopping == _state)
			{
				std::scoped_lock lock{ *_protocol_mutex };
				_protocol.receive_packet(address, packet);
				if (ProtocolState::Disconnected == _protocol.state())
					_state = State::Stopped;
			}
		}

		void run()
		{
			start_timer();
			_io.run();
		}

		void start_timer()
		{
			_timer.expires_from_now(TimerResolution);
			_timer.async_wait([this](boost::system::error_code const& error)
			{
				if (!error)
				{
					if (State::Started == _state || State::Stopping == _state)
					{
						std::scoped_lock lock{ *_protocol_mutex };
						_protocol.set_time(::steady_clock_time());
						if (ProtocolState::Disconnected == _protocol.state())
						{
							if (State::Stopping == _state)
								_state = State::Stopped;
							else if (auto const address = _protocol.remote_address())
								_protocol.set_remote_address(*address);
						}
					}
					start_timer();
				}
			});
		}

	private:
		enum class State
		{
			Stopped,
			Started,
			Stopping,
		};

		EndpointCallbacks& _callbacks;
		State _state = State::Stopped;
		boost::asio::io_service _io;
		boost::asio::steady_timer _timer{ _io };
		UdpTransport _transport;
		UdpProtocol _protocol;
		std::shared_ptr<std::recursive_mutex> _protocol_mutex = std::make_shared<std::recursive_mutex>();
		std::thread _thread;
	};

	std::unique_ptr<Endpoint> create_endpoint(std::uint32_t token, std::uint16_t port, EndpointCallbacks& callbacks)
	{
		return std::make_unique<EndpointImpl>(token, port, callbacks);
	}
}
