#include "udp.h"

namespace uce
{
	UdpTransport::UdpTransport(std::uint16_t port, boost::asio::io_service& io, ReceiveCallback const& receive_callback)
		: _socket{ io, { boost::asio::ip::udp::v4(), port } }
		, _receive_callback{ receive_callback }
	{
		receive();
	}

	UdpTransport::~UdpTransport() noexcept = default;

	bool UdpTransport::send_packet(Address const& address, Bytes header, Bytes payload)
	{
		if (!payload.empty())
		{
			std::array<boost::asio::const_buffer, 2> buffers
			{
				boost::asio::buffer(header.data(), header.size()),
				boost::asio::buffer(payload.data(), payload.size()),
			};
			return _socket.send_to(buffers, address) == header.size() + payload.size();
		}
		else
			return _socket.send_to(boost::asio::buffer(header.data(), header.size()), address) == header.size();
	}

	void UdpTransport::receive()
	{
		_socket.async_receive_from(boost::asio::buffer(_buffer), _remote_address, [this](boost::system::error_code const& error, std::size_t bytes_received)
		{
			if (!error)
			{
				_receive_callback(_remote_address, { _buffer.data(), bytes_received });
				receive();
			}
		});
	}
}
