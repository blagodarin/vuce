#include "reassembler.h"

#include <cassert>
#include <cstring>
#include <utility>

#include "packet.h"

namespace uce
{
	bool Reassembler::push_fragment(FragmentPacketHeader const& header, Bytes payload)
	{
		return 0 == header._fragment_offset
			? push_first_fragment(header, payload)
			: push_next_fragment(header, payload);
	}

	void Reassembler::reset() noexcept
	{
		_buffer.reset();
		_buffer_offset = 0;
		_next_sequence = 0;
	}

	Buffer Reassembler::take_message() noexcept
	{
		assert(has_message());
		_next_sequence = 0;
		_buffer_offset = 0;
		return std::move(_buffer);
	}

	bool Reassembler::push_first_fragment(FragmentPacketHeader const& header, Bytes payload)
	{
		if (header._message_size < payload.size())
			return false;
		_buffer.reset(header._message_size);
		std::memcpy(_buffer.data(), payload.data(), payload.size());
		_buffer_offset = payload.size();
		_next_sequence = header._sequence_number + 1;
		return true;
	}

	bool Reassembler::push_next_fragment(FragmentPacketHeader const& header, Bytes payload) noexcept
	{
		if (!_next_sequence
			|| header._sequence_number != _next_sequence
			|| header._message_size != _buffer.size()
			|| header._fragment_offset != _buffer_offset
			|| payload.size() > header._message_size - header._fragment_offset)
			return false;
		std::memcpy(_buffer.data() + _buffer_offset, payload.data(), payload.size());
		_buffer_offset += payload.size();
		++_next_sequence;
		return true;
	}
}
