#pragma once

#include <cstddef>

namespace uce
{
	class Buffer
	{
	public:
		Buffer() noexcept = default;
		Buffer(const Buffer&) = delete;
		Buffer(Buffer&&) noexcept;
		~Buffer() noexcept;
		Buffer& operator=(const Buffer&) = delete;
		Buffer& operator=(Buffer&&) noexcept;

		Buffer(const void* data, std::size_t size);

		static Buffer by_reference(void* data, std::size_t size) noexcept;

		std::size_t capacity() const noexcept { return _capacity; }
		std::byte* data() noexcept { return _data; }
		const std::byte* data() const noexcept { return _data; }
		bool empty() const noexcept { return !_size; }
		void reset() noexcept;
		void reset(std::size_t size);
		std::size_t size() const noexcept { return _size; }

	private:
		std::byte* _data = nullptr;
		std::size_t _capacity = 0;
		std::size_t _size = 0;
	};

	bool operator==(const Buffer&, const Buffer&) noexcept;
	inline bool operator!=(const Buffer& a, const Buffer& b) noexcept { return !(a == b); }
}
